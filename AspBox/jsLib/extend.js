// JavaScript Document

//字符转换为unicode
//(e.g. chr2Unicode('中文') == '2D4E8765'; chr2Unicode('a1') == '61003100')
function chr2Unicode(str) {
	if ('' != str) {
		var st, t, i;
		st = '';
		for (i = 1; i <= str.length; i ++){
			t = str.charCodeAt(i - 1).toString(16);
			if (t.length < 4)
				while(t.length <4)
					t = '0'.concat(t);
			t = t.slice(2, 4).concat(t.slice(0, 2))
			st = st.concat(t);
		}
		return(st.toUpperCase());
	}
	else {
		return('');
	}
}
//unicode转换为字符
//(e.g. unicode2Chr('2D4E8765') == '中文'; unicode2Chr('61003100') == 'a1')
function unicode2Chr(str) {
	if ('' != str) {
		var st, t, i
		st = '';
		for (i = 1; i <= str.length/4; i ++){
			t = str.slice(4*i-4, 4*i-2);
			t = str.slice(4*i-2, 4*i).concat(t);
			st = st.concat('%u').concat(t);
		}
		st = unescape(st);
		return(st);
	}
	else
		return('');
} 