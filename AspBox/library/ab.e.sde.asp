<%
'######################################################################
'## ab.e.sde.asp
'## -------------------------------------------------------------------
'## Feature     :   SDE Encryption
'## Version     :   v1.0
'## Author      :   Lajox(lajox@19www.com)
'## Update Date :   2012/06/10 22:27
'## Description :   AspBox SDE Encryption Block
'######################################################################

Class Cls_AB_E_SDE
	Private Sub Class_Initialize() : End Sub
	Private Sub Class_Terminate() : End Sub

	'@ ******************************************************************
	'@ 过程名:  AB.E.SDE.E(str) {简写为： AB.E.SDE(str) }
	'@ 返  回:  加密后的字符串(由 A-Za-z0-9 等字符组成)
	'@ 作  用:  对字符串进行加密(使用soMDA加密算法,加密失败则返回原始值)
	'==Param==============================================================
	'@ str  : 待加密的字符串 # [String]
	'==DEMO==============================================================
	'@ AB.E.SDE.E("aspbox") => wc0uc9kr9Co7ld4qv4
	'@ ******************************************************************

	Public Default Function E(Byval s)
		E = SoMDA(s)
	End Function

	'@ ******************************************************************
	'@ 过程名:  AB.E.SDE.D(str)
	'@ 返  回:  对由soMDA加密算法加密的字符串进行解密还原
	'@ 作  用:  解密由soMDA加密算法的字符串(解密失败则返回原始值)
	'==Param==============================================================
	'@ str  : 待解密的字符串 # [String]
	'==DEMO==============================================================
	'@ AB.E.SDE.D(AB.E.SDE.E("aspbox")) '返回值: aspbox
	'@ ******************************************************************

	Public Function D(Byval s)
		D = SoDeMDA(s)
	End Function

	Public Function SoMDA(Byval s)
	    '**********************************************
	    '@ SoMDA(s): String
	    '@ 加密字符串，加密长度为原来的3倍
	    '==TODO========================================
	    '@ 新增字典版本号，最大限度提高加密效果
	    '@ 解决偶尔加密位数错误的BUG
	    '@ 新增加密失败则返回原始值
	    '@ 新增对中文及特殊字符串的加密支持
	    '==FLOW========================================
	    '@ ascii[==32~126]
	    '@ Fix[==288~444](256~318)
	    '@ POSD[==28801~44462]定位,前置和，后置随机数字典位置
	    '@ MOD 252 [==114073~176110]
	    '@ POSD[==aa0~9y9]
	    '==DESC========================================
	    '@ 支持英文,数字,符号加密,支持汉字加密
	    '==DEMO========================================
	    '@ 无
	    '**********************************************
		On Error Resume Next
		Dim temp:temp = s
	    If Isemptyvalue(temp) Then Exit function
		temp=Escape(temp)
	    If IsMDAStandar(temp)=False Then
	        SoMDA=temp
	        Exit function
	    End IF
	    Dim strOutput,intAscChar,intPos,intMod,intRemain,strFixChar,i,intRnd
	    Dim strDict10,strDict26,strDict62
	    strDict62="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	    strDict26="abcdefghijklmnopqrstuvwxyz"
	    strDict10="0123456789"
	    randomize
	    For i=1 To len(temp)
	        '截字
	        intAscChar=Asc(Mid(temp,i,1))
	        '随机数
	        intRnd=256+round(rnd()*62)
	        '和
	        intAscChar=intAscChar+intRnd
	        '定位
	        intPos=319-intRnd
	        intPos=right("0" & intPos,2)
	        intAscChar=intAscChar & intPos
	        '取模求余
	        intMod=intAscChar \ 252
	        intRemain=intAscChar Mod 252
	        intRemain=right("000" & intRemain,3)
	        '定位
	        strFixChar=Mid(strDict62,177-intMod,1) & Mid(strDict26,left(intRemain,2)+1,1) & Mid(strDict10,right(intRemain,1)+1,1)
	        strOutput=strOutput & strFixChar
	    Next
	    SoMDA=strOutput
		On Error Goto 0
	End Function

	Public Function SoDeMDA(Byval s)
		On Error Resume Next
		Dim temp:temp = s
	    If Isemptyvalue(temp) Then Exit Function
	    If IsDEMDAStandar(temp)=False Then
	        SoDeMDA=temp
	        Exit function
	    End IF
	    If len(temp) Mod 3>0 Then : SoDeMDA=temp: Exit Function : End IF
	    Dim strOutput,strFixChar,intMod,intRemain,intAscChar,intRnd,i,intFixChar
	    Dim strDict10,strDict26,strDict62
	    strDict62="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	    strDict26="abcdefghijklmnopqrstuvwxyz"
	    strDict10="0123456789"
	    For i=1 To len(temp) Step 3
	        strFixChar=Mid(temp,i,3)
	        intMod=177-instr(strDict62,left(strFixChar,1))
	        intRemain=instr(strDict26,Mid(strFixChar,2,1))-1
	        intRemain=intRemain & instr(strDict10,right(strFixChar,1))-1
	        intFixChar=intMod*252 + intRemain
	        intAscChar=left(intFixChar,3)
	        intRnd=right(intFixChar,2)
	        intRnd=319-intRnd
	        intAscChar=intAscChar-intRnd
	        strOutput=strOutput & chr(intAscChar)
	    Next
		SoDeMDA=UnEscape(strOutput)
		If Err.Number<>0 Then : SoDeMDA=temp: Err.Clear : End IF
		On Error Goto 0
	End Function

	'是否符合somda规范
	Private Function IsDEMDAStandar(Byval s)
	    If s="" or isnull(s) Then
	        IsDEMDAStandar=False
	        Exit Function
	    End IF
	    Dim regex : Set regex=new regexp
	    regex.IgnoreCase=True
	    regex.Pattern="[^a-z0-9]+"
	    IsDEMDAStandar=Not regex.Test(s)
		Set regex = Nothing
	End Function

	'是否符合MDA加密规范
	Private Function IsMDAStandar(Byval s)
	    Dim blnOutput,i,strChar,intAsc
	    blnOutput=True
	    For i=1 to len(s)
	        If i=len(s) Then Exit For
	        strChar=mid(s,i,1)
	        intAsc=asc(strChar)
	        If intAsc<32 or intAsc>126 Then
	            blnOutput=False
	            Exit For
	        End IF
	    Next
	    IsMDAStandar=blnOutput
	End Function

	'检查参数是否为空
	Private Function IsEmptyValue(Byval s)
		If isnull(s) or s="" Then
			IsEmptyValue=True
		Else
			IsEmptyValue=False
		End IF
	End Function
End Class
%>