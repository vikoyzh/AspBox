<%
'######################################################################
'## ab.time.asp
'## -------------------------------------------------------------------
'## Feature     :   AspBox Time Block
'## Version     :   v1.0.1
'## Author      :   Lajox(lajox@19www.com)
'## Update Date :   2013/03/13 15:30
'## Description :   AspBox Time Block
'######################################################################

Class Cls_AB_Time

	Private sGMTTimeZone_, sTimeZone_
	Private date_
	Private firstweekofyear_
	Private firstdayofweek_

	Private Sub Class_Initialize()
		sGMTTimeZone_ = "+08:00" '时区与UTC的偏移量[即(+/-)HH:MM,如+00:00,中国的时区是+8,则为+08:00]
		sTimeZone_ = +8
		date_ = Date '默认当前日期
		firstdayofweek_ = 2 'vbMonday
		firstweekofyear_ = 1 '由 1 月 1 日所在的星期开始。
	End Sub

	Private Sub Class_Terminate()

	End Sub

	'@ *****************************************************************************************
	'@ 过程名:  AB.Time.GMTTimeZone [= t]
	'@ 返  回:  无
	'@ 作  用:  设置/获取[属性] UTC时区偏移量
	'==DESC=====================================================================================
	'@ 参数 t(可选): String (字符串) 时区与UTC的偏移量[即(+/-)HH:MM,如+00:00,中国的时区是+8,则为+08:00]
	'==DEMO=====================================================================================
	'@ AB.Time.GMTTimeZone = "+08:00"
	'@ AB.C.PrintCn AB.Time.GMTTimeZone
	'@ *****************************************************************************************

	Public Property Let GMTTimeZone(Byval t)
		If Not IsNull(t) and t<>"" Then sGMTTimeZone_ = t
		sTimeZone_ = GetTimeZone__(2)
	End Property

	Public Property Get GMTTimeZone()
		GMTTimeZone = sGMTTimeZone_
	End Property

	'@ *****************************************************************************************
	'@ 过程名:  AB.Time.TimeZone [= t]
	'@ 返  回:  --
	'@ 作  用:  设置/获取[属性] TimeZone时区
	'==DESC=====================================================================================
	'@ 参数 t(可选): 数字 如 +8 、 +7.5 、 -1
	'==DEMO=====================================================================================
	'@ AB.Time.TimeZone = +8
	'@ AB.C.PrintCn AB.Time.TimeZone
	'@ *****************************************************************************************

	Public Property Let TimeZone(Byval t)
		If Not IsNull(t) and t<>"" Then sTimeZone_ = CDbl(t)
		sGMTTimeZone_ = ToGMTTimeZone__(t)
	End Property

	Public Property Get TimeZone()
		TimeZone = sTimeZone_
	End Property

	'@ *****************************************************************************************
	'@ 过程名:  AB.Time.setDate = t
	'@ 返  回:  无
	'@ 作  用:  设置[属性] 设置初始化日期
	'==DESC=====================================================================================
	'@ 参数 t: 日期
	'==DEMO=====================================================================================
	'@ AB.Time.setDate = Date
	'@ *****************************************************************************************

	Public Property Let setDate(Byval t)
		On Error Resume Next
		Dim temp: temp = date_
		If Not IsNull(temp) and temp<>"" Then temp = t
		If IsNumeric(temp) Then:IF CLng(temp)<=12 Then temp=temp&"-1":End IF
		If Not IsNull(temp) and temp<>"" Then date_ = CDate(temp)
		On Error Goto 0
	End Property

	'@ *****************************************************************************************
	'@ 过程名:  AB.Time.FirstWeekOfYear = n
	'@ 返  回:  无
	'@ 作  用:  设置[属性] 初始设定一年的第一周，若未设定则表示一月一日那一周为一年的第一周。
	'==DESC=====================================================================================
	'@ 参数 n: Integer (整数)
	'@ 设定值如下(详细设置请参照VBS手册)：
	'@  0 使用 >API 的设定值。
	'@  1 一月一日那一周为一年的第一周
	'@  2 至少包括四天的第一周为一年的第一周
	'@  3 包括七天的第一周为一年的第一周
	'==DEMO=====================================================================================
	'@ AB.Time.FirstWeekOfYear = 1
	'@ *****************************************************************************************

	Public Property Let FirstWeekOfYear(Byval n)
		If IsNumeric(temp) Then:IF CLng(n)>=1 and CLng(n)<=31 Then firstweekofyear_ = CLng(n):End IF
	End Property

	'@ *****************************************************************************************
	'@ 过程名:  AB.Time.FirstDayOfWeek = n
	'@ 返  回:  无
	'@ 作  用:  设置[属性] 初始设定每周第一天为星期几，若未设定表示为星期天。
	'==DESC=====================================================================================
	'@ 参数 n: Integer (整数)
	'@ 设定值如下(详细设置请参照VBS手册)：
	'@ 0 使用 >API 的设定值。
	'@ 1:星期天,2:星期一,3:星期二,4:星期三,5:星期四,6:星期五,7:星期六
	'==DEMO=====================================================================================
	'@ AB.Time.FirstDayOfWeek = 2
	'@ *****************************************************************************************

	Public Property Let FirstDayOfWeek(Byval n)
		If IsNumeric(temp) Then:IF CLng(n)>=1 and CLng(n)<=7 Then firstdayofweek_ = CLng(n):End IF
	End Property

	'@ *****************************************************************************************
	'@ 过程名:  AB.Time.GetWeekDate(y, w, d)
	'@ 返  回:  日期
	'@ 作  用:  计算第几周的星期几是几号
	'==DESC=====================================================================================
	'@ 参数 y: Integer (整数) 年
	'@ 参数 w: Integer (整数) 周
	'@ 参数 d: Integer (整数) week星期(星期一:1 星期天:7)
	'==DEMO=====================================================================================
	'@ AB.C.Print AB.Time.GetWeekDate(2012,1,1) '表示2012年的第一周的星期一，返回值 2011-12-26
	'@ *****************************************************************************************

	Public Function GetWeekDate(Byval y, Byval w, Byval d)
		Dim NewYearDay
		NewYearDay = Cdate(y & "-1-1") '元旦
		GetWeekDate = ((NewYearDay - Weekday(NewYearDay, firstdayofweek_)) + (w - 1) * 7 + d)
	End Function

	'@ *****************************************************************************************
	'@ 过程名:  AB.Time.WeekFirstDay(t)
	'@ 返  回:  日期
	'@ 作  用:  某日所在的周的第一天的日期
	'==DESC=====================================================================================
	'@ 参数 t: 时间
	'==DEMO=====================================================================================
	'@ AB.C.Print AB.Time.WeekFirstDay("10-01")
	'@ 上面表示今年(2011年)的10月1日所在的周的第一天的日期，返回值 2011-9-26
	'@ *****************************************************************************************

	Public Function WeekFirstDay(Byval t)
		On Error Resume Next
		Dim temp: temp = t
		If IsNull(temp) Or temp="" Then temp = date_
		If IsNumeric(temp) Then:IF CLng(temp)<=12 Then temp=Day(Now)&"-"&temp:End IF
		Dim time:time = CDate(temp)
		WeekFirstDay = GetWeekDate(Year(time), DatePart("ww", time, firstdayofweek_, firstweekofyear_), 1)
		On Error Goto 0
	End Function

	'@ *****************************************************************************************
	'@ 过程名:  AB.Time.WeekFirstDay(t)
	'@ 返  回:  日期
	'@ 作  用:  某日所在的周的第最后一天的日期
	'==DESC=====================================================================================
	'@ 参数 t: 时间
	'==DEMO=====================================================================================
	'@ AB.C.Print AB.Time.WeekLastDay("10-01")
	'@ 上面表示今年(2011年)的10月1日所在的周的最后一天的日期，返回值 2011-10-2
	'@ *****************************************************************************************

	Public Function WeekLastDay(Byval t)
		On Error Resume Next
		Dim temp: temp = t
		If IsNull(temp) Or temp="" Then temp = date_
		If IsNumeric(temp) Then:IF CLng(temp)<=31 Then temp=Day(Now)&"-"&temp:End IF
		Dim time:time = CDate(temp)
		WeekLastDay = GetWeekDate(Year(time), DatePart("ww", time, firstdayofweek_, firstweekofyear_), 7)
		On Error Goto 0
	End Function

	'@ *****************************************************************************************
	'@ 过程名:  AB.Time.GetMonthDayCount(t)
	'@ 返  回:  Integer (整数)
	'@ 作  用:  获得某年某月的天数
	'==DESC=====================================================================================
	'@ 参数 t: 时间
	'==DEMO=====================================================================================
	'@ AB.C.Print AB.Time.GetMonthDayCount("2012-02") '返回值 29
	'@ *****************************************************************************************

	Public Function GetMonthDayCount(Byval t)
		On Error Resume Next
		Dim temp: temp = t
		If IsNull(temp) Or temp="" Then temp = date_
		If IsNumeric(temp) Then:IF CLng(temp)<=12 Then temp=temp&"-1":End IF
		Dim time:time = CDate(temp)
		GetMonthDayCount = DateDiff("d", time, DateAdd("m", 1, time))
		On Error Goto 0
	End Function

	'@ *****************************************************************************************
	'@ 过程名:  AB.Time.GetMonthFirstDay(t)
	'@ 返  回:  日期
	'@ 作  用:  得到某年某月的第一天
	'==DESC=====================================================================================
	'@ 参数 t: 时间
	'==DEMO=====================================================================================
	'@ AB.C.Print AB.Time.GetMonthFirstDay("2012-02") '返回值 2012-2-1
	'@ *****************************************************************************************

	Public Function GetMonthFirstDay(Byval t)
		On Error Resume Next
		Dim temp: temp = t
		If IsNull(temp) Or temp="" Then temp = date_
		If IsNumeric(temp) Then:IF CLng(temp)<=12 Then temp=temp&"-1":End IF
		Dim time:time = CDate(temp)
		GetMonthFirstDay = Cdate( Year(time) & "-" & Month(time) & "-1")
		On Error Goto 0
	End Function

	'@ *****************************************************************************************
	'@ 过程名:  AB.Time.GetMonthLastDay(t)
	'@ 返  回:  日期
	'@ 作  用:  得到某年的某月的最后一天
	'==DESC=====================================================================================
	'@ 参数 t: 时间
	'==DEMO=====================================================================================
	'@ AB.C.Print AB.Time.GetMonthLastDay("2012-02") '返回值 2012-2-29
	'@ *****************************************************************************************

	Public Function GetMonthLastDay(Byval t)
		On Error Resume Next
		Dim temp: temp = t
		If IsNull(temp) Or temp="" Then temp = date_
		If IsNumeric(temp) Then:IF CLng(temp)<=12 Then temp=temp&"-1":End IF
		Dim time:time = CDate(temp)
		GetMonthLastDay = Cdate( Year(temp) & "-"&Month(temp) & "-" & DateDiff("d", temp, DateAdd("m", 1, temp)))
		On Error Goto 0
	End Function

	'@ *****************************************************************************************
	'@ 过程名:  AB.Time.GetDayOrdinal(intDay)
	'@ 返  回:  String (字符串)
	'@ 作  用:  得到某月的某日的英文单词的后缀
	'==DESC=====================================================================================
	'@ 参数 intDay: Integer (数字)
	'==DEMO=====================================================================================
	'@ AB.C.Print AB.Time.GetDayOrdinal(3) '返回值 rd
	'@ *****************************************************************************************

	Public Function GetDayOrdinal(byVal intDay)
		On Error Resume Next
		Dim strOrd
		Select Case intDay
			Case 1, 21, 31
				strOrd = "st"
			Case 2, 22
				strOrd = "nd"
			Case 3, 23
				strOrd = "rd"
			Case Else
				strOrd = "th"
		End Select
		GetDayOrdinal = strOrd
		On Error Goto 0
	End Function

	'@ *****************************************************************************************
	'@ 过程名:  AB.Time.Zodiac(intDay)
	'@ 返  回:  String (字符串)
	'@ 作  用:  计算(某年)所属生肖, 可以根据生日的那年计算出所属生肖
	'==DESC=====================================================================================
	'@ 参数 intDay: Integer (数字)
	'==DEMO=====================================================================================
	'@ AB.C.Print AB.Time.Zodiac(1986) '返回值: 虎
	'@ *****************************************************************************************

    Function Zodiac(bYear)
        If bYear > 0 Then
            Dim ZodiacList
            ZodiacList = Array("猴", "鸡", "狗", "猪", "鼠", "牛", "虎", "兔", "龙", "蛇", "马", "羊")
            Zodiac = ZodiacList(bYear Mod 12)
        End If
    End Function

	'@ *****************************************************************************************
	'@ 过程名:  AB.Time.Constellation(Birth)
	'@ 返  回:  String (字符串)
	'@ 作  用:  计算(某月某日)所属星座
	'==DESC=====================================================================================
	'@ 参数 Birth: 日期 (某月某日)
	'==DEMO=====================================================================================
	'@ AB.C.Print AB.Time.Constellation("08-15") '返回值: 狮子座
	'@ *****************************************************************************************

    Function Constellation(Byval Birth)
        If Year(Birth) <1951 Or Year(Birth) > 2049 Then Exit Function
        Dim BirthDay, BirthMonth
        BirthDay = Day(Birth)
        BirthMonth = Month(Birth)
		Dim tmp : tmp = ""
        Select Case BirthMonth
            Case 1 If BirthDay>= 21 Then : tmp = tmp & "水瓶座" : Else : tmp = tmp & "魔羯座" : End If
            Case 2 If BirthDay>= 20 Then : tmp = tmp & "双鱼座" : Else : tmp = tmp & "水瓶座" : End If
            Case 3 If BirthDay>= 21 Then : tmp = tmp & "白羊座" : Else : tmp = tmp & "双鱼座" : End If
            Case 4 If BirthDay>= 21 Then : tmp = tmp & "金牛座" : Else : tmp = tmp & "白羊座" : End If
            Case 5 If BirthDay>= 22 Then : tmp = tmp & "双子座" : Else : tmp = tmp & "金牛座" : End If
            Case 6 If BirthDay>= 22 Then : tmp = tmp & "巨蟹座" : Else : tmp = tmp & "双子座" : End If
            Case 7 If BirthDay>= 23 Then : tmp = tmp & "狮子座" : Else : tmp = tmp & "巨蟹座" : End If
            Case 8 If BirthDay>= 24 Then : tmp = tmp & "处女座" : Else : tmp = tmp & "狮子座" : End If
            Case 9 If BirthDay>= 24 Then : tmp = tmp & "天秤座" : Else : tmp = tmp & "处女座" : End If
            Case 10 If BirthDay>= 24 Then : tmp = tmp & "天蝎座" : Else : tmp = tmp & "天秤座" : End If
            Case 11 If BirthDay>= 23 Then : tmp = tmp & "射手座" : Else : tmp = tmp & "天蝎座" : End If
            Case 12 If BirthDay>= 22 Then : tmp = tmp & "魔羯座" : Else : tmp = tmp & "射手座" : End If
            Case Else tmp = ""
        End Select
		Constellation = tmp
    End Function

	'@ *****************************************************************************************
	'@ 函数名:  AB.Time.ServerTime()
	'@ 返  回:  Int (整型)
	'@ 作  用:  获取服务端时间(标准时间格式)
	'==DESC=====================================================================================
	'@ 参数: 无
	'==DEMO=====================================================================================
	'@ AB.C.Print AB.Time.ServerTime '输出: 2013-03-13 15:29:01
	'@ *****************************************************************************************

	Public Function ServerTime()
		ServerTime = CDate(stdTime(Now))
	End Function

	'@ *****************************************************************************************
	'@ 函数名:  AB.Time.LocalTime()
	'@ 返  回:  Int (整型)
	'@ 作  用:  获取客户端(本地电脑)时区
	'==DESC=====================================================================================
	'@ 参数: 无
	'==DEMO=====================================================================================
	'@ AB.C.Print AB.Time.LocalTime '输出: 2013-03-13 15:29:05
	'@ *****************************************************************************************

	Public Function LocalTime()
		Dim jstr
		jstr = "" &_
		"var pa = 'yyyy-MM-dd hh:mm:ss';" &_
		"Date.prototype.getFormattedDate = function(pattern){" &_
		"	function getFullStr(i){" &_
		"		return i>9?''+i:'0'+i;" &_
		"	}" &_
		"	pattern = pattern.replace(/yyyy/,this.getFullYear())" &_
		"		.replace(/MM/,getFullStr(this.getMonth()+1))" &_
		"		.replace(/dd/,getFullStr(this.getDate()))" &_
		"		.replace(/hh/,getFullStr(this.getHours()))" &_
		"		.replace(/mm/,getFullStr(this.getMinutes()))" &_
		"		.replace(/ss/,getFullStr(this.getSeconds()));" &_
		"	return pattern;" &_
		"};" &_
		"var t1 = (new Date()).toString();" &_
		"var t2 = (new Date()).getFormattedDate(pa);" &_
		"var localTime = t2;"
		AB.Use "Sc"
		Dim sc : Set sc = AB.Sc.New
		sc.Lang = "js"
		sc.Add jstr
		Dim jso : Set jso = sc.Object
		LocalTime = CDate(jso.localTime)
		Set jso = Nothing
		Set sc = Nothing
	End Function

	'@ *****************************************************************************************
	'@ 函数名:  AB.Time.ServerTimezone()
	'@ 返  回:  Int (整型)
	'@ 作  用:  获取服务端时区
	'==DESC=====================================================================================
	'@ 参数: 无
	'==DEMO=====================================================================================
	'@ AB.C.Print AB.Time.ServerTimezone '返回值: 8 '即为+8时区
	'@ *****************************************************************************************

	Public Function ServerTimezone()
		ServerTimezone = GetInfoTimeZone__(0)
	End Function

	'@ *****************************************************************************************
	'@ 函数名:  AB.Time.LocalTimezone()
	'@ 返  回:  Int (整型)
	'@ 作  用:  获取客户端(本地电脑)时区
	'==DESC=====================================================================================
	'@ 参数: 无
	'==DEMO=====================================================================================
	'@ AB.C.Print AB.Time.LocalTimezone '返回值: 8 '即为+8时区
	'@ *****************************************************************************************

	Public Function LocalTimezone()
		LocalTimezone = GetInfoTimeZone__(1)
	End Function

	'@ *****************************************************************************************
	'@ 过程名:  AB.Time.DateTimeToGMT(t)
	'@ 返  回:  String (字符串) GMT时间格式的字符串
	'@ 作  用:  转换时间为GMT(RFC822)格式时间函数
	'==DESC=====================================================================================
	'@ 参数 t: 日期
	'==DEMO=====================================================================================
	'@ AB.C.Print AB.Time.DateTimeToGMT("2011-12-28 03:52:27") '返回值: Wes, 28 Dec 2011 03:52:27 +0800
	'@ *****************************************************************************************

	Public Function DateTimeToGMT(Byval t)
		Dim dWeek,dMonth
		Dim strZero,strZone
		strZero="00"
        If sTimeZone_ > 0 Then
            strZone = "+"&Right("0"&sTimeZone_, 2)&"00"
        Else
            strZone = "-"&Right("0"&sTimeZone_, 2)&"00"
        End If
		dWeek=Array("Sun","Mon","Tue","Wes","Thu","Fri","Sat")
		dMonth=Array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec")
		DateTimeToGMT = dWeek(WeekDay(t)-1)&", "&Right(strZero&Day(t),2)&" "&dMonth(Month(t)-1)&" "&Year(t)&" "&Right(strZero&Hour(t),2)&":"&Right(strZero&Minute(t),2)&":"&Right(strZero&Second(t),2)&" "&strZone
	End Function

	'@ *****************************************************************************************
	'@ 过程名:  AB.Time.GMTToDateTime(t)
	'@ 返  回:  日期
	'@ 作  用:  根据RFC822的标准,将RFC822日期字符串转换成标准时间(By Lajox 2011-12-12 01:26)
	'==DESC=====================================================================================
	'@ 参数 t: (GMT格式)时间
	'==DEMO=====================================================================================
	'@ AB.C.Print AB.Time.GMTToDateTime("Wes, 28 Dec 2011 03:52:27 +0800") '返回值: 2011-12-28 03:52:27
	'@ *****************************************************************************************

	Public Function GMTToDateTime(Byval t)
		On Error Resume Next
		Dim sYear, sMonth, sDay, sHour, sMinute, sSecond, sGMT, nTimeZone
		Dim ZoneOffset : ZoneOffset = 0
		Dim arrWeek : arrWeek = Array("Sun","Mon","Tue","Wes","Thu","Fri","Sat")
		Dim arrMonth : arrMonth = Array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec")
		Dim I, K, Temp : I = 0
		Dim strData : strData = Trim(t)
		If Mid(strData,4,1)="," Then strData = Trim(Mid(strData,5,Len(strData))) '删掉无用的星期信息
		If Instr(strData," ")>0 Then
			sDay = Split(strData," ")(0) '天
			strData = Trim(AB.C.RegReplace(strData,"^\d+\s*",""))
		End If
		If Instr(strData," ")>0 Then
			Temp = Split(strData," ")(0) '月
			For I=0 To UBound(arrMonth)
				If Temp = arrMonth(I) Then
					sMonth = I+1
					Exit For
				End If
			Next
			strData = Trim(Right(strData,Len(strData)-4))
		End If
		If Instr(strData," ")>0 Then
			sYear = Split(strData," ")(0) '年
			strData = Trim(AB.C.RegReplace(strData,"^\d+\s*",""))
		End If
		If Instr(strData,":")>0 Then
			sHour = Split(strData,":")(0) '时
			strData = Trim(AB.C.RegReplace(strData,"^\d+\:",""))
		End If
		If Instr(strData,":")>0 Then
			sMinute = Split(strData,":")(0) '分
			strData = Trim(AB.C.RegReplace(strData,"^\d+\:",""))
		End If
		sSecond = Split(strData," ")(0) '秒
		strData = Trim(AB.C.RegReplace(strData,"^\d+\s*",""))
		If strData<>"" Then sGMT = "" & strData 'GMT
		Dim nZoneOffset : nZoneOffset = 0 '最后处理时区
		If Len(sGMT)=3 Then 'UT,GMT,EST,EDT,CST,CDT,MST,MDT,PST,PDT
            If UCase(Left(sGMT,1)) = "E" Then
				If UCase(Mid(sGMT,2,1)) = "S" Then
					nZoneOffset = -5 * 60 * 60
				ElseIF UCase(Mid(sGMT,2,1)) = "D" Then
					nZoneOffset = -4 * 60 * 60
				End If
			ElseIf UCase(Left(sGMT,1)) = "C" Then
				If UCase(Mid(sGMT,2,1)) = "S" Then
					nZoneOffset = -6 * 60 * 60
				ElseIF UCase(Mid(sGMT,2,1)) = "D" Then
					nZoneOffset = -5 * 60 * 60
				End If
			ElseIf UCase(Left(sGMT,1)) = "M" Then
				If UCase(Mid(sGMT,2,1)) = "S" Then
					nZoneOffset = -7 * 60 * 60
				ElseIF UCase(Mid(sGMT,2,1)) = "D" Then
					nZoneOffset = -6 * 60 * 60
				End If
			ElseIf UCase(Left(sGMT,1)) = "P" Then
				If UCase(Mid(sGMT,2,1)) = "S" Then
					nZoneOffset = -8 * 60 * 60
				ElseIF UCase(Mid(sGMT,2,1)) = "D" Then
					nZoneOffset = -7 * 60 * 60
				End If
			Else
				'GMT and UT, the nZoneOffset = 0
			End If
		ElseIf Len(sGMT)=1 Then '军方时区
			If Char(sGMT) >= Char("A") and Char(sGMT) <= Char("F") Then
				nZoneOffset = -60 * 60 * Char(sGMT) - Char("A") + 1
			ElseIf Char(sGMT) >= Char("K") and Char(sGMT) <= Char("M") Then
				nZoneOffset = -60 * 60 * Char(sGMT) - Char("A") + 2
			ElseIf Char(sGMT) >= Char("N") and Char(sGMT) <= Char("Y") Then
				nZoneOffset = -60 * 60 * Char(sGMT) - Char("N") + 1
			ElseIf Char(sGMT) >= Char("a") and Char(sGMT) <= Char("f") Then
				nZoneOffset = -60 * 60 * Char(sGMT) - Char("a") + 1
			ElseIf Char(sGMT) >= Char("k") and Char(sGMT) <= Char("m") Then
				nZoneOffset = -60 * 60 * Char(sGMT) - Char("a") + 2
			ElseIf Char(sGMT) >= Char("n") and Char(sGMT) <= Char("y") Then
				nZoneOffset = -60 * 60 * Char(sGMT) - Char("n") + 1
			End If
		Else '数字时区(如 +0830 hours+min. HHMM)
			Dim sk : sk = 1 : If Left(sGMT,1) = "-" Then sk = -1
			nTimeZone = Cint(sGMT)
            nZoneOffset = sk * (60 * 60 * (Abs(nTimeZone) \ 100) + 60 * (Abs(nTimeZone) - (Abs(nTimeZone) \ 100) * 100))
		End If
		Dim sTime
		If sMonth="" Then
			sTime = Trim(t)
		Else
			ZoneOffset = -(nZoneOffset - GetTimeZone__(3))
			sTime = sYear&"-"&sMonth&"-"&sDay&" "&sHour&":"&sMinute&":"&sSecond
			sTime = DateAdd("s", ZoneOffset, Cdate(sTime) )
			sTime = FormatTime(sTime, "Y-m-d H:i:s")
		End If
		GMTToDateTime = sTime
		On Error Goto 0
	End Function

	'@ *****************************************************************************************
	'@ 过程名:  AB.Time.ToUnixEpoch(t)
	'@ 别  名:  AB.Time.ToID(t) 或 AB.Time.TimeID(t)
	'@ 返  回:  Integer (整数) 时间戳
	'@ 作  用:  获取时间戳, 把时间转化为Unix时间戳
	'==DESC=====================================================================================
	'@ 参数 t: 时间
	'==DEMO=====================================================================================
	'@ AB.Time.TimeZone = +8 '缺省时区: +8
	'@ AB.C.Print AB.Time.ToID("2011-12-28 03:52:27") '返回值(会跟随 AB.Time.TimeZone值 变化而变化): 1325015547
	'@ *****************************************************************************************

    Public Function ToUnixEpoch(Byval t)
		If AB.C.isNul(t) Then t = Now()
        ToUnixEpoch = DateDiff("s", "1970-1-1 00:00:00", CDate(t)) - getMistiming("1970-1-1 00:00:00")
    End Function
    Public Function ToID(Byval t) : ToID = ToUnixEpoch(t) : End Function
    Public Function TimeID(Byval t) : TimeID = ToUnixEpoch(t) : End Function

	'@ *****************************************************************************************
	'@ 过程名:  AB.Time.FromUnixEpoch(n)
	'@ 别  名:  AB.Time.FromID(n) 或 AB.Time.FromTimeID(n)
	'@ 返  回:  时间
	'@ 作  用:  把Unix时间戳转为获取普通时间
	'==DESC=====================================================================================
	'@ 参数 n: Integer (整数) 时间戳
	'==DEMO=====================================================================================
	'@ AB.Time.TimeZone = +8 '缺省时区: +8
	'@ AB.C.Print AB.Time.FromID(1325015547) '返回值(会跟随 AB.Time.TimeZone值 变化而变化): 2011-12-28 3:52:27
	'@ *****************************************************************************************

    Function FromUnixEpoch(Byval n)
        FromUnixEpoch = DateAdd("s", CLng(n) + getMistiming("1970-1-1 00:00:00"), "1970-1-1 00:00:00")
    End Function
    Public Function FromID(Byval n) : ToID = FromUnixEpoch(n) : End Function
    Public Function FromTimeID(Byval n) : FromTimeID = FromUnixEpoch(n) : End Function

	'@ *****************************************************************************************
	'@ 过程名:  AB.Time.DateTimeToUnixTime(t, ，缺省则默认显示为 ab.time 设置的 时区)
	'@ 返  回:  Integer (数字) 时间戳
	'@ 作  用:  将时间格式转化为UNIX时间戳
	'==DESC=====================================================================================
	'@ 参数 t: String (字符串) 时间
	'@ 参数 zone: Integer (整数) 时区，若值为空则默认显示为ab.time设置的时区
	'==DEMO=====================================================================================
	'@ AB.C.Print AB.Time.DateTimeToUnixTime("2011-4-24 12:51:11", +8) '返回值: 1303620671
	'@ *****************************************************************************************

	Public Function DateTimeToUnixTime(Byval t, Byval zone)
		Dim NowTime:NowTime=Now()
		Dim strTime:strTime=t
		Dim intTimeZone:intTimeZone=zone
		If IsEmpty(strTime) or Not IsDate(strTime) Then strTime = Now()
		If IsEmpty(intTimeZone) or Not isNumeric(intTimeZone) Then intTimeZone = sTimeZone_
		NowTime = DateAdd("h",-intTimeZone,CDate(strTime))
		DateTimeToUnixTime = DateDiff("s","1970-1-1 0:0:0", NowTime)
	End Function

	'@ *****************************************************************************************
	'@ 过程名:  AB.Time.UnixTimeToDateTime(n, zone)
	'@ 返  回:  时间
	'@ 作  用:  将UNIX时间戳转化为时间格式
	'==DESC=====================================================================================
	'@ 参数 n: Integer (整数) 时间戳
	'@ 参数 zone: Integer (整数) 时区，若值为空则默认显示为ab.time设置的时区
	'==DEMO=====================================================================================
	'@ AB.C.Print AB.Time.UnixTimeToDateTime("1303620671", +8) '返回值: 2011-4-24 12:51:11
	'@ *****************************************************************************************

	Public Function UnixTimeToDateTime(Byval n, Byval zone)
		Dim NowTime:NowTime=Now()
		Dim intTime:intTime=n
		Dim intTimeZone:intTimeZone=zone
		If IsEmpty(intTime) or Not IsNumeric(intTime) Then
			UnixTimeToDateTime = NowTime
			Exit Function
		End If
		If IsEmpty(intTime) or Not IsNumeric(intTimeZone) Then intTimeZone = sTimeZone_
		NowTime = DateAdd("s", intTime, "1970-1-1 0:0:0")
		UnixTimeToDateTime = DateAdd("h", intTimeZone, CDate(NowTime))
	End Function

	'@ *****************************************************************************************
	'@ 过程名:  AB.Time.gfdtLocal2GMT(LocalDate)
	'@ 返  回:  时间
	'@ 作  用:  Local时间向GMT时间的转化
	'==DESC=====================================================================================
	'@ 参数 LocalDate: String (字符串) 时间
	'==DEMO=====================================================================================
	'@ AB.Time.TimeZone = +8 '缺省时区: +8
	'@ AB.C.Print AB.Time.gfdtLocal2GMT(Now) '输出值: 2013-2-21 0:54:40
	'@ *****************************************************************************************

	Public Function gfdtLocal2GMT(ByVal LocalDate)
		On Error Resume Next
		Dim intTime,intMinute,dtTempDate,dtOutDate
		Dim SG_strTimeZone
		SG_strTimeZone = "GMT+08:00" '具体可通过查看本地计算机Local时间(日期和时间属性-时区选项卡)
		If GetTimeZone__(0) <> "" Then SG_strTimeZone = "GMT" & GetTimeZone__(0) & ""
		If Left(SG_strTimeZone, 3)<>"GMT" Then
			gfdtLocal2GMT = LocalDate
			Exit Function
		Else
			Select Case Mid(SG_strTimeZone, 4, 1)
			Case "+"
				intTime = Mid(SG_strTimeZone, 5, 2)
				intMinute = Mid(SG_strTimeZone, 8, 2)
				If 0 <= intTime And intTime < 24 Then
					If 0 <= intMinute And intMinute < 60 Then
						dtTempDate = DateAdd("h", -intTime, CDate(LocalDate))
						dtOutDate = DateAdd("n", -intMinute, CDate(dtTempDate))
					Else
						gfdtLocal2GMT = LocalDate
						Exit Function
					End If
				Else
					gfdtLocal2GMT = LocalDate
					Exit Function
				End If
			Case "-"
				intTime = Mid(SG_strTimeZone, 5, 2)
				intMinute = Mid(SG_strTimeZone, 8, 2)
				If 0 <= intTime And intTime < 24 Then
					If 0 <= intMinute And intMinute < 60 Then
						dtTempDate = DateAdd("h", intTime, CDate(LocalDate))
						dtOutDate = DateAdd("n", intMinute, CDate(dtTempDate))
					Else
						gfdtLocal2GMT = LocalDate
						Exit Function
					End If
				Else
					gfdtLocal2GMT = LocalDate
					Exit Function
				End If
			Case Else
				gfdtLocal2GMT = LocalDate
				Exit Function
			End Select
		End If
		gfdtLocal2GMT = cdate(dtOutDate)
		On Error Goto 0
	End Function

	'@ *****************************************************************************************
	'@ 过程名:  AB.Time.FormatTime(t, p) ASP格式化日期
	'@ 返  回:  字符串 [String]
	'@ 作  用:  ASP格式化日期，把php的date函数格式在asp中的实现，有些非ASP能完成的格式符号预留出来了
	'==DESC=====================================================================================
	'@ 参数 t : 原时间字串
	'@ 参数 p : 要输出的时间格式(模板)
	'@ =====
	'@ 支持GMT时区定义
	'@ 格林威治时间的格式取消了函数内附加的[GMT]字符串，改用在格式字符串里定义，参考范例
	'@ 支持RFC 822协议的日期格式
	'@ 增加转义字符支持
	'==DEMO=====================================================================================
	'@ AB.C.PrintCn AB.Time.FormatTime(Now(), "Y-m-d H:i:s") 'yyyy-mm-dd hh24:mi:ss格式
	'@ AB.C.PrintCn AB.Time.FormatTime(Now(), "Y年m月d日 H时i分s秒") '有前导0的日期
	'@ AB.C.PrintCn AB.Time.FormatTime(Now(), "Y年n月j日 g时i分s秒 A") '无前导0的日期
	'@ AB.C.PrintCn AB.Time.FormatTime(Now(), "r") 'RFC 822协议的日期格式
	'@ =格林威治时间
	'@ AB.C.PrintCn AB.Time.FormatGTime(Now(), 0)
	'@ AB.C.PrintCn AB.Time.FormatGTime(Now(), 1)
	'@ *****************************************************************************************

	Public Function FormatTime(ByVal dtmValue, ByVal strFmt)
		Dim pWeek
		Dim pWeekFull
		Dim pMonth
		Dim pMonthFull
		Dim ret, tmp
		Dim i, l
		Dim y, m, d, h, n, s, w
		pWeek = Array("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat")
		pWeekFull = Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday")
		pMonth = Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec")
		pMonthFull = Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December")
		y = Year(dtmValue)
		m = Month(dtmValue)
		d = Day(dtmValue)
		h = Hour(dtmValue)
		n = Minute(dtmValue)
		s = Second(dtmValue)
		w = Weekday(dtmValue)
		l = Len(strFmt)
		ReDim ret(l - 1)
		For i = 1 To l
			tmp = Mid(strFmt, i, 1)
			Select Case tmp
			Case "a":
				'a 小写的上午和下午值 am 或 pm
				ret(i - 1) = AB.C.IIf(h < 12, "am", "pm")
			Case "A":
				'A 大写的上午和下午值 AM 或 PM
				ret(i - 1) = AB.C.IIf(h < 12, "AM", "PM")
			Case "B":
				'B Swatch Internet 标准时 000 到 999
			Case "d":
				'd 月份中的第几天，有前导零的 2 位数字 01 到 31
				ret(i - 1) = FillZero(d)
			Case "D":
				'D 星期中的第几天，文本表示，3 个字母 Mon 到 Sun
				ret(i - 1) = pWeek(w - 1)
			Case "F":
				'F 月份，完整的文本格式，例如 January 或者 March January 到 December
				ret(i - 1) = pMonthFull(m - 1)
			Case "g":
				'g 小时，12 小时格式，没有前导零 1 到 12
				ret(i - 1) = (h Mod 12)
			Case "G":
				'G 小时，24 小时格式，没有前导零 0 到 23
				ret(i - 1) = h
			Case "h":
				'h 小时，12 小时格式，有前导零 01 到 12
				ret(i - 1) = FillZero(h Mod 12)
			Case "H":
				'H 小时，24 小时格式，有前导零 00 到 23
				ret(i - 1) = FillZero(h)
			Case "i":
				'i 有前导零的分钟数 00 到 59>
				ret(i - 1) = FillZero(n)
			Case "I":
				'ret(i - 1) = tt->tm_isdst'I 是否为夏令时 如果是夏令时为 1，否则为 0
			Case "j":
				'j 月份中的第几天，没有前导零 1 到 31
				ret(i - 1) = d
			Case "l":
				'l（“L”的小写字母） 星期几，完整的文本格式 Sunday 到 Saturday
				ret(i - 1) = pWeekFull(w - 1)
			Case "L":
				'L 是否为闰年 如果是闰年为 1，否则为 0
				ret(i - 1) = AB.C.IIf((y Mod 4 = 0 And y Mod 100 <>0) Or y Mod 400 = 0, 1, 0)
			Case "m":
				'm 数字表示的月份，有前导零 01 到 12
				ret(i - 1) = FillZero(m)
			Case "M":
				'M 三个字母缩写表示的月份 Jan 到 Dec
				ret(i - 1) = pMonth(m - 1)
			Case "n":
				'n 数字表示的月份，没有前导零 1 到 12
				ret(i - 1) = m
			Case "O":
				'O 与格林威治时间相差的小时数 例如：+0200
				ret(i - 1) = "$1$200"
				If sTimeZone_ < 0 Then
					ret(i - 1) = Replace(ret(i - 1), "$1", "-")
				Else
					ret(i - 1) = Replace(ret(i - 1), "$1", "+")
				End If
				ret(i - 1) = Replace(ret(i - 1), "$2", FillZero(Abs(sTimeZone_)))
			Case "r":
				'r RFC 822 格式的日期 例如：Thu, 21 Dec 2000 16:01:07 +0200
				ret(i - 1) = FormatTime(dtmValue, "D, j M Y H:i:s O")
			Case "s":
				's 秒数，有前导零 00 到 59>
				ret(i - 1) = FillZero(s)
			Case "S":
				'S 每月天数后面的英文后缀，2 个字符 st，nd，rd 或者 th。可以和 j 一起用。
			Case "t":
				't 给定月份所应有的天数 28 到 31
				If m = 1 Or m = 3 Or m = 5 Or m = 7 Or m = 8 Or m = 10 Or m = 12 Then
					ret(i - 1) = 31
				ElseIf m = 4 Or m = 6 Or m = 9 Or m = 11 Then
					ret(i - 1) = 30
				Else
					ret(i - 1) = AB.C.IIf((y Mod 4 = 0 And y Mod 100 <>0) Or y Mod 400 = 0, 29, 28)
				End If
			Case "T":
				'T 时区
			Case "U":
				'U 从 Unix 纪元（January 1 1970 00:00:00 GMT）开始至今的秒数 参见 time()
				ret(i - 1) = DateDiff("s", "1970-01-01 00:00:00", dtmValue)
			Case "w":
				'w 星期中的第几天，数字表示 0（表示星期天）到 6（表示星期六）
				ret(i - 1) = w
			Case "W":
				'W ISO-8601 格式年份中的第几周，每周从星期一开始（PHP 4.1.0 新加的） 例如：42（当年的第 42 周）
			Case "Y":
				'Y 4 位数字完整表示的年份 例如：1999 或 2003
				ret(i - 1) = y
			Case "y":
				'y 2 位数字表示的年份 例如：99 或 03
				ret(i - 1) = Right(y, 2)
			Case "z":
				'z 年份中的第几天 0 到 366
				ret(i - 1) = DateDiff("d", y & "-01-01", dtmValue)
			Case "Z":
				'Z 时差偏移量的秒数。UTC 西边的时区偏移量总是负的，UTC 东边的时区偏移量总是正的。
			Case "/":
				'转义字符
				i = i + 1
				ret(i - 1) = Mid(strFmt, i, 1)
			Case Else
				ret(i - 1) = tmp
			End Select
		Next
		FormatTime = Join(ret, Empty)
	End Function

	'@intType 格式类型（值0则加GMT后缀；值为1则加UTC后缀）
	'@格式日期采用类型格式，不再使用字符串格式
	Public Function FormatGTime(ByVal dtmValue, ByVal intType)
		FormatGTime = FormatTime(DateAdd("h", 0 - sTimeZone_, dtmValue), AB.C.IIf(intType = 0, "D, j M Y H:i:s /G/M/T", "D, j M Y H:i:s /U/T/C"))
	End Function

	'时间格式化函数
	' e.g. AB.Time.FormatDate(Now,"%Y-%m-%d")
	' Des:
		' Accepts strDate as a valid date/time,
		' strFormat as the output template.
		' The function finds each item in the
		' template and replaces it with the
		' relevant information extracted from strDate.
		' You are free to use this code provided the following line remains
		' www.adopenstatic.com/resources/code/formatdate.asp
		' --------------
		' Template items
		' %m Month as a decimal no. 2
		' %M Month as a padded decimal no. 02
		' %B Full month name February
		' %b Abbreviated month name Feb
		' %d Day of the month eg 23
		' %D Padded day of the month eg 09
		' %O ordinal of day of month (eg st or rd or nd)
		' %j Day of the year 54
		' %Y Year with century 1998
		' %y Year without century 98
		' %w Weekday as integer (0 is Sunday)
		' %a Abbreviated day name Fri
		' %A Weekday Name Friday
		' %H Hour in 24 hour format 24
		' %h Hour in 12 hour format 12
		' %N Minute as an integer 01
		' %n Minute as optional if minute <> 00
		' %S Second as an integer 55
		' %P AM/PM Indicator PM

	Public Function FormatDate(byVal strDate, byVal strFormat)
		On Error Resume Next
		Dim intPosItem
		Dim int12HourPart
		Dim str24HourPart
		Dim strMinutePart
		Dim strSecondPart
		Dim strAMPM
		' Insert Month Numbers
		strFormat = Replace(strFormat, "%m", DatePart("m", strDate), 1, -1, vbBinaryCompare)
		' Insert Padded Month Numbers
		strFormat = Replace(strFormat, "%M", Right("0" & DatePart("m", strDate), 2), 1, -1, vbBinaryCompare)
		' Insert non-Abbreviated Month Names
		strFormat = Replace(strFormat, "%B", MonthName(DatePart("m", strDate), False), 1, -1, vbBinaryCompare)
		' Insert Abbreviated Month Names
		strFormat = Replace(strFormat, "%b", MonthName(DatePart("m", strDate), True), 1, -1, vbBinaryCompare)
		' Insert Day Of Month
		strFormat = Replace(strFormat, "%d", DatePart("d", strDate), 1, -1, vbBinaryCompare)
		' Insert Padded Day Of Month
		strFormat = Replace(strFormat, "%D", Right ("0" & DatePart("d", strDate), 2), 1, -1, vbBinaryCompare)
		' Insert Day of Month ordinal (eg st, th, or rd)
		strFormat = Replace(strFormat, "%O", GetDayOrdinal(Day(strDate)), 1, -1, vbBinaryCompare)
		' Insert Day of Year
		strFormat = Replace(strFormat, "%j", DatePart("y", strDate), 1, -1, vbBinaryCompare)
		' Insert Long Year (4 digit)
		strFormat = Replace(strFormat, "%Y", DatePart("yyyy", strDate), 1, -1, vbBinaryCompare)
		' Insert Short Year (2 digit)
		strFormat = Replace(strFormat, "%y", Right(DatePart("yyyy", strDate), 2), 1, -1, vbBinaryCompare)
		' Insert Weekday as Integer (eg 0 = Sunday)
		strFormat = Replace(strFormat, "%w", DatePart("w", strDate, 1), 1, -1, vbBinaryCompare)
		' Insert Abbreviated Weekday Name (eg Sun)
		strFormat = Replace(strFormat, "%a", WeekdayName(DatePart("w", strDate, 1), True), 1, -1, vbBinaryCompare)
		' Insert non-Abbreviated Weekday Name
		strFormat = Replace(strFormat, "%A", WeekdayName(DatePart("w", strDate, 1), False), 1, -1, vbBinaryCompare)
		' Insert Hour in 24hr format
		str24HourPart = DatePart("h", strDate)
		If Len(str24HourPart) < 2 Then str24HourPart = "0" & str24HourPart
		strFormat = Replace(strFormat, "%H", str24HourPart, 1, -1, vbBinaryCompare)
		' Insert Hour in 12hr format
		int12HourPart = DatePart("h", strDate) Mod 12
		If int12HourPart = 0 Then int12HourPart = 12
		strFormat = Replace(strFormat, "%h", int12HourPart, 1, -1, vbBinaryCompare)
		' Insert Minutes
		strMinutePart = DatePart("n", strDate)
		If Len(strMinutePart) < 2 Then strMinutePart = "0" & strMinutePart
		strFormat = Replace(strFormat, "%N", strMinutePart, 1, -1, vbBinaryCompare)
		' Insert Optional Minutes
		If CInt(strMinutePart) = 0 Then
		strFormat = Replace(strFormat, "%n", "", 1, -1, vbBinaryCompare)
		Else
		If CInt(strMinutePart) < 10 Then strMinutePart = "0" & strMinutePart
		strMinutePart = ":" & strMinutePart
		strFormat = Replace(strFormat, "%n", strMinutePart, 1, -1, vbBinaryCompare)
		End If
		' Insert Seconds
		strSecondPart = DatePart("s", strDate)
		If Len(strSecondPart) < 2 Then strSecondPart = "0" & strSecondPart
		strFormat = Replace(strFormat, "%S", strSecondPart, 1, -1, vbBinaryCompare)
		' Insert AM/PM indicator
		If DatePart("h", strDate) >= 12 Then
		strAMPM = "PM"
		Else
		strAMPM = "AM"
		End If
		strFormat = Replace(strFormat, "%P", strAMPM, 1, -1, vbBinaryCompare)
		FormatDate = strFormat
		On Error Goto 0
	End Function

	'@ ******************************************************
	'@ AB.Time.TakeTime(t, p) 返回字符窜
	'@ 格式化显示时间
	'@ author: Lajox; version: 1.0.0 (2011-05-28);
	'==DESC==================================================
	'@ p :	设定对应形式
	'@ (1) : 		转化为形式: yyyy-mm-dd
	'@ (2) : 		转化为形式: yyyy-mm-dd hh:nn:ss
	'@ (3) : 		转化为形式: yyyy-mm-dd hh:nn
	'@ (4) : 		转化为形式: yyyy年mm月dd日
	'@ (5) : 		转化为形式: yyyy/mm/dd
	'@ (6) : 		转化为形式: yyyymmdd
	'@ (7) : 		转化为形式: yymmdd
	'@ (8) : 		转化为形式: mmdd
	'@ (9) : 		转化为形式: mm月dd日
	'@ (10) : 		转化为形式: mm/dd
	'@ (11) : 		转化为形式: yyyy年mm月dd日 hh时nn分ss秒
	'@ (12) : 		转化为形式: yyyy年mm月dd日 hh时nn分
	'@ (13) : 		转化为形式: yy-mm-dd hh:nn:ss
	'@ (14) : 		转化为形式: yy-mm-dd hh:nn
	'@ (15) : 		转化为形式: yyyy年mm月dd日 hh:nn:ss
	'@ (16) : 		转化为形式: yyyy年mm月dd日 hh:nn
	'@ (17) : 		转化为形式: yy年mm月dd日 hh:nn
	'@ (18) : 		转化为形式: mm月dd日 hh:nn
	'@ (19) : 		转化为形式: mm-dd hh:nn
	'@ (20) : 		转化为形式: hh:nn:ss
	'@ (21) : 		转化为形式: hh:nn
	'@ (22) : 		转化为形式: hh时nn分ss秒
	'@ (23) : 		转化为形式: hh时nn分
	'@ "101" : 		转化为形式: yyyy年mm月dd日 星期×
	'@ "102" : 		转化为形式: 星期×
	'@ "103" : 		转化为形式: 周×
	'==DEMO==================================================
	'@ AB.Time.TakeTime(Now(),2) '返回值 2011-05-28 23:02:41
	'@ *****************************************************

	Public Function TakeTime(Byval t, Byval p)
		Dim temp
		Dim ly,lm,ld,lh,ln,ls '年字符串（长 2011），月字符串（长 01），日字符串（长 01），时字符串（长 01），分字符串（长 01），秒字符串（长 01）
		Dim sy,sm,sd,sh,sn,ss '年字符串（短 11），月字符串（短 1），日字符串（短 1），时字符串（短 1），分字符串（短 1），秒字符串（短 1）
		IF IsNull(t) then
			p=0
		Else
			ly=DatePart("yyyy",t)
			If DatePart("m",t)>9 then : lm=DatePart("m",t) : else : lm="0"&DatePart("m",t) : End If
			If DatePart("d",t)>9 then : ld=DatePart("d",t) : else : ld="0"&DatePart("d",t) : End If
			If DatePart("h",t)>9 then : lh=DatePart("h",t) : else : lh="0"&DatePart("h",t) : End If
			If DatePart("n",t)>9 then : ln=DatePart("n",t) : else : ln="0"&DatePart("n",t) : End If
			If DatePart("s",t)>9 then : ls=DatePart("s",t) : else : ls="0"&DatePart("s",t) : End If
			sy = right(DatePart("yyyy",t),2)
			sm=DatePart("m",t) : sd=DatePart("d",t) : sh=DatePart("h",t) : sn=DatePart("n",t) : ss=DatePart("s",t)
		End IF
		Dim weekname : weekname = WeekdayName(Weekday(t)) '返回: 星期一、星期二、...星期六、星期天
		Dim weeknow : weeknow = split("周日,周一,周二,周三,周四,周五,周六",",")(Weekday(DateTime)-1) '返回: 周一、周二、...周六、周日
		Select Case p
			Case 0 : temp = t
			Case 1 : temp = ly&"-"&lm&"-"&ld
			Case 2 : temp = ly&"-"&lm&"-"&ld&" "&lh&":"&ln&":"&ls
			Case 3 : temp = ly&"-"&lm&"-"&ld&" "&lh&":"&ln
			Case 4 : temp = ly&"年"&lm&"月"&ld&"日"
			Case 5 : temp = ly&"/"&lm&"/"&ld&""
			Case 6 : temp = ly&""&lm&""&ld&""
			Case 7 : temp = sy&""&lm&""&ld&""
			Case 8 : temp = lm&""&ld&""
			Case 9 : temp = lm&"月"&ld&"日"
			Case 10 : temp = lm&"/"&ld&""
			Case 11 : temp = ly&"年"&lm&"月"&ld&"日 "&lh&"时"&ln&"分"&ls&"秒"
			Case 12 : temp = ly&"年"&lm&"月"&ld&"日 "&lh&"时"&ln&"分"
			Case 13 : temp = sy&"-"&lm&"-"&ld&" "&lh&":"&ln&":"&ls
			Case 14 : temp = sy&"-"&lm&"-"&ld&" "&lh&":"&ln
			Case 15 : temp = ly&"年"&lm&"月"&ld&"日 "&lh&":"&ln&":"&ls&""
			Case 16 : temp = ly&"年"&lm&"月"&ld&"日 "&lh&":"&ln&""
			Case 17 : temp = sy&"年"&lm&"月"&ld&"日 "&lh&":"&ln&""
			Case 18 : temp = lm&"月"&ld&"日 "&lh&":"&ln
			Case 19 : temp = lm&"-"&ld&" "&lh&":"&ln
			Case 20 : temp = lh&":"&ln&":"&ls&""
			Case 21 : temp = lh&":"&ln&""
			Case 22 : temp = lh&"时"&ln&"分"&ls&"秒"
			Case 23 : temp = lh&"时"&ln&"分"
			Case 101 : temp = sy&"年"&lm&"月"&ld&"日 " & weekname
			Case 102 : temp = weekname
			Case 103 : temp = weeknow
		End Select
		TakeTime = temp
	End Function

	'@ *******************************************************
	'@ AB.Time.DateToStr(t, p)
	'@ 日期转换函数
	'==DESC===================================================
	'@ 参数 t : 时间
	'@ 参数 p : 格式
	'==DEMO===================================================
	'@ AB.C.Print AB.Time.DateToStr(Now,"Y-m-d")
	'@ *******************************************************

	Public Function DateToStr(Byval t,Byval n)
		Dim DateMonth,DateDay,DateHour,DateMinute,DateWeek,DateSecond
		Dim FullWeekday,shortWeekday,Fullmonth,Shortmonth,TimeZone1,TimeZone2
		TimeZone1=GetTimeZone__(1)
		TimeZone2=GetTimeZone__(0)
		FullWeekday=Array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday")
		shortWeekday=Array("Sun","Mon","Tue","Wed","Thu","Fri","Sat")
		Fullmonth=Array("January","February","March","April","May","June","July","August","September","October","November","December")
		Shortmonth=Array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec")
		DateMonth=Month(t)
		DateDay=Day(t)
		DateHour=Hour(t)
		DateMinute=Minute(t)
		DateWeek=weekday(t)
		DateSecond=Second(t)
		If Len(DateMonth)<2 Then DateMonth="0"&DateMonth
		If Len(DateDay)<2 Then DateDay="0"&DateDay
		If Len(DateMinute)<2 Then DateMinute="0"&DateMinute
		Select Case n
		Case "Y-m-d"
			DateToStr=Year(t)&"-"&DateMonth&"-"&DateDay
		Case "Y-m-d H:I A"
			Dim DateAMPM
			If DateHour>12 Then
				DateHour=DateHour-12
				DateAMPM="PM"
			Else
				DateHour=DateHour
				DateAMPM="AM"
			End If
			If Len(DateHour)<2 Then DateHour="0"&DateHour
			DateToStr=Year(t)&"-"&DateMonth&"-"&DateDay&" "&DateHour&":"&DateMinute&" "&DateAMPM
		Case "Y-m-d H:I:S"
			If Len(DateHour)<2 Then DateHour="0"&DateHour
			If Len(DateSecond)<2 Then DateSecond="0"&DateSecond
			DateToStr=Year(t)&"-"&DateMonth&"-"&DateDay&" "&DateHour&":"&DateMinute&":"&DateSecond
		Case "YmdHIS"
			DateSecond=Second(t)
			If Len(DateHour)<2 Then DateHour="0"&DateHour
			If Len(DateSecond)<2 Then DateSecond="0"&DateSecond
			DateToStr=Year(t)&DateMonth&DateDay&DateHour&DateMinute&DateSecond
		Case "ym"
			DateToStr=Right(Year(t),2)&DateMonth
		Case "d"
			DateToStr=DateDay
		Case "ymd"
			DateToStr=Right(Year(t),4)&DateMonth&DateDay
		Case "mdy"
			Dim DayEnd
			select Case DateDay
			 Case 1
			  DayEnd="st"
			 Case 2
			  DayEnd="nd"
			 Case 3
			  DayEnd="rd"
			 Case Else
			  DayEnd="th"
			End Select
			DateToStr=Fullmonth(DateMonth-1)&" "&DateDay&DayEnd&" "&Right(Year(t),4)
		Case "w,d m y H:I:S"
			DateSecond=Second(t)
			If Len(DateHour)<2 Then DateHour="0"&DateHour
			If Len(DateSecond)<2 Then DateSecond="0"&DateSecond
			DateToStr=shortWeekday(DateWeek-1)&","&DateDay&" "& Left(Fullmonth(DateMonth-1),3) &" "&Right(Year(t),4)&" "&DateHour&":"&DateMinute&":"&DateSecond&" "&TimeZone1
		Case "y-m-dTH:I:S"
			If Len(DateHour)<2 Then DateHour="0"&DateHour
			If Len(DateSecond)<2 Then DateSecond="0"&DateSecond
			DateToStr=Year(t)&"-"&DateMonth&"-"&DateDay&"T"&DateHour&":"&DateMinute&":"&DateSecond&TimeZone2
		Case Else
			If Len(DateHour)<2 Then DateHour="0"&DateHour
			DateToStr=Year(t)&"-"&DateMonth&"-"&DateDay&" "&DateHour&":"&DateMinute
		End Select
	End Function

	'@ *******************************************************
	'@ AB.Time.stdTime(t)
	'@ 获取标准时间格式
	'==DESC===================================================
	'@ 参数 t : 时间
	'==DEMO===================================================
	'@ 示例：AB.C.Print AB.Time.stdTime(Now) '输出：2013-02-19 11:31:26
	'@ *******************************************************

	Public Function stdTime(Byval t)
		Dim tm : tm = t : If IsNull(t) Or t="" Then tm = Now
		Dim p : p = "Y-M-D h:n:s"
		Dim s : s = p & ""
		tm = CDate(tm)
		s = Replace(s,"yyyy",year(tm))
		s = Replace(s,"yy",right(year(tm),2))
		s = Replace(s,"Y",year(tm))
		s = Replace(s,"y",year(tm))
		s = Replace(s,"M",right("0" & month(tm),2))
		s = Replace(s,"mm",right("0" & month(tm),2))
		s = Replace(s,"m",month(tm))
		s = Replace(s,"D",right("0" & day(tm),2))
		s = Replace(s,"dd",right("0" & day(tm),2))
		s = Replace(s,"d",day(tm))
		s = Replace(s,"H",right("0" & hour(tm),2))
		s = Replace(s,"hh",right("0" & hour(tm),2))
		s = Replace(s,"h",hour(tm))
		s = Replace(s,"N",right("0" & minute(tm),2))
		s = Replace(s,"nn",right("0" & minute(tm),2))
		s = Replace(s,"n",minute(tm))
		s = Replace(s,"S",right("0" & second(tm),2))
		s = Replace(s,"ss",right("0" & second(tm),2))
		s = Replace(s,"s",second(tm))
		stdTime = s
	End Function

	'@ ***********************************************************************
	'@ AB.Time.TimeAdd(style,numeric,datetime)
	'@ 时间增减函数(高级),并返回标准格式
	'@ author: Lajox; version: 1.0.0 (2011-10-6);
	'==DESC===================================================================
	'@ datetime : 原时间
	'@ numeric : 增减数字 (n>0则向前增加, n<0则向后减少, n=0返回原时间)
	'@ style : 定义格式 (s:秒, n:分, h:时, d:天, w:一周的日数, ww:周, m:月, q:季度, y:一年的日数, yyyy:年)
	'==DEMO==================================================
	'@ AB.C.Print AB.Time.TimeAdd("s",50,"2011-2-28 23:59:09") '返回值 2011-02-28 23:59:59
	'@ ***********************************************************************

	Public Function TimeAdd(Byval p, Byval k, Byval t)
		On Error Resume Next
		Dim x:x=k
		Dim style:style="s"
		Dim tm:tm=t
		Dim time:time=tm
		Dim y, m, d, h, n, s, y_, m_, d_, h_, n_, s_
		y = year(tm): m = month(tm): d = day(tm): h = hour(tm): n = minute(tm): s = second(tm)
		y_=y: m_=Right("0"&m,2): d_=Right("0"&d,2): h_=Right("0"&h,2): n_=Right("0"&n,2): s_=Right("0"&s,2)
		time = ( y_ & "-" & m_ & "-" & d_ ) & " " & ( h_ & ":" & n_ & ":" & s_) : time = CDate(time)
		Dim temp
		If IsNumeric(x) Then x=CLng(x)
		If Not IsNull(p) and p<>"" Then style=p&""
		Select Case style
			Case "s" 's:秒
				temp = DateAdd("s", x, time)
			Case "n" 'n:分钟
				temp = DateAdd("n", x, time)
			Case "h" 'h:小时
				temp = DateAdd("h", x, time)
			Case "d" 'd:日
				temp = DateAdd("d", x, time)
			Case "w" 'w:一周的日数
				temp = DateAdd("ww", x, time)
			Case "ww" 'ww:周
				temp = DateAdd("ww", x, time)
			Case "m" 'm:月
				temp = DateAdd("m", x, time)
			Case "q" 'q:季度
				temp = DateAdd("q", x, time)
			Case "yyyy" 'yyyy:年
				temp = DateAdd("yyyy", x, time)
			Case Else
				temp = time
		End Select
		temp = ( year(temp) & "-" & Right("0"&month(temp),2) & "-" & Right("0"&day(temp),2) ) & " " & ( Right("0"&hour(temp),2) & ":" & Right("0"&minute(temp),2) & ":" & Right("0"&second(temp),2))
		TimeAdd = temp
		On Error Goto 0
	End Function

	'@ ***********************************************************************
	'@ AB.Time.TimeAddSecond(datetime,n) 返回: 时间格式"2011-02-28 22:13:59"
	'@ 时间增减函数
	'@ author: Lajox; version: 1.0.0 (2011-05-28);
	'==DESC===================================================================
	'@ datetime : 要改变的时间
	'@ n : 增减的秒数 (n>0则向前增加时间, n<0则向后减少时间, n=0返回原时间)
	'==DEMO==================================================
	'@ AB.C.Print AB.Time.TimeAddSecond("2011-2-28 23:59:09",50) '返回值 2011-02-28 23:59:59
	'@ ***********************************************************************

	Public Function TimeAddSecond(Byval t, Byval n)
		dim ls_date,ls_getstr
		dim year__,month__,day__,hour__,minute__,second__
		year__ = year(t):month__ = month(t):day__ = day(t)
		hour__ = hour(t):minute__ = minute(t):second__ = second(t)
		dim DateTime '获取日期
		'DateTime = FormatDateTime(t,vbShortDate) '返回当前日期
		dim NewDateTime '新日期
		DateTime = CDate(year__ & "-" & month__ & "-" & day__ & "")
		Dim yr_n,mt_n,dy_n,hr_n,mn_n,sc_n '增量
		yr_n=0:mt_n=0:dy_n=0:hr_n=0:mn_n=0:sc_n=0
		n=CLng(n)
		if CLng(n)=0 Then
			year__=CLng(year__):month__=CLng(month__):day__=CLng(day__):hour__=CLng(hour__):minute__=CLng(minute__):second__=CLng(second__)
		elseif CLng(n)>0 Then
			second__=CLng(second__)+CLng(n)
			if second__>=60 then
				sc_n = CLng(CLng(second__) \ 60) '分进位
				second__ = CLng(CLng(second__) mod 60) '获取真实秒数(过满60则取模)
			end if
			minute__=CLng(minute__)+CLng(sc_n)
			if minute__>=60 then
				mn_n = CLng(CLng(minute__) \ 60) '时进位
				minute__ = CLng(CLng(minute__) mod 60) '获取真实分钟数(过满60则取模)
			end if
			hour__=CLng(hour__)+CLng(mn_n)
			if hour__>=24 then
				hr_n = CLng(CLng(hour__) \ 24) '天进位
				hour__ = CLng(CLng(hour__) mod 24) '获取真实时钟数(过满24则取模)
			end if
			NewDateTime = DateAdd("d", hr_n, DateTime)
			year__ = year(NewDateTime):month__ = month(NewDateTime):day__ = day(NewDateTime)
		else
			second__=CLng(second__)+CLng(n)
			if second__<0 then
				sc_n = CLng(CLng(0-CLng(second__)) \ 60)+1 '分退位
				second__ = 60-CLng(CLng(0-CLng(second__)) mod 60) '获取真实秒数(过满60则取模)
				if second__=60 then second__=0:sc_n=sc_n-1
			end if
			minute__=CLng(minute__)-CLng(sc_n)
			if minute__<0 then
				mn_n = CLng(CLng(0-CLng(minute__)) \ 60)+1 '时退位
				minute__ = 60-CLng(CLng(0-CLng(minute__)) mod 60) '获取真实分钟数(过满60则取模)
				if minute__=60 then minute__=0:mn_n=mn_n-1
			end if
			hour__=CLng(hour__)-CLng(mn_n)
			if hour__<0 then
				hr_n = CLng(CLng(0-CLng(hour__)) \ 24)+1 '天退位
				hour__ = 24-CLng(CLng(0-CLng(hour__)) mod 24) '获取真实时钟数(过满24则取模)
				if hour__=24 then hour__=0:hr_n=hr_n-1
			end if
			NewDateTime = DateAdd("d", -hr_n, DateTime)
			year__ = year(NewDateTime):month__ = month(NewDateTime):day__ = day(NewDateTime)
		end if
		year__=year__:month__=Right("0"&month__,2):day__=Right("0"&day__,2):hour__=Right("0"&hour__,2):minute__=Right("0"&minute__,2):second__=Right("0"&second__,2)
		dim datetimes__
		'datetimes__ = datetimes
		datetimes__ = ( year__ & "-" & month__ & "-" & day__ ) & " " & ( hour__ & ":" & minute__ & ":" & second__)
		'datetimes__ = FormatDateTime(datetimes__,vbLongDate) & " " & FormatDateTime(datetimes__,vbLongTime)
		'if isdate(datetimes) then
			ls_date = datetimes__
			ls_getstr = ls_date
			TimeAddSecond=ls_getstr
		'end if
	End Function

	'-------------------------------------------------------------------------------------------
	'# AB.Time.TimeNum(p, t)
	'# @return: integer
	'# @dowhat: 生成时间的整数
	'--DESC------------------------------------------------------------------------------------
	'# @param p: [string] (字符串) 定义类型 p=0 到秒 p=1 到分钟 p=2 到小时 p=3 到天 p=4 到月
	'# @param t: [string] (字符串) 时间
	'--DEMO------------------------------------------------------------------------------------
	'# ab.c.printcn AB.Time.TimeNum(0, "") '=> 62593585903
	'# ab.c.printcn AB.Time.TimeNum(1, "2012-01-01") '=> 1043065440
	'------------------------------------------------------------------------------------------

	Public Function TimeNum(Byval p, Byval t)
		If IsNull(t) Or Trim(t)="" Or Trim(t&"")="0" Then t=Now : t = CDate(t)
		If p=0 Then TimeNum=Year(t)*12*30*24*60*60+Month(t)*30*24*60*60+Day(t)*24*60*60+Hour(t)*60*60+Minute(t)*60+Second(t)
		If p=1 Then TimeNum=Year(t)*12*30*24*60+Month(t)*30*24*60+Day(t)*24*60+Hour(t)*60+Minute(t)
		If p=2 Then TimeNum=Year(t)*12*30*24+Month(t)*30*24+Day(t)*24+Hour(t)
		If p=3 Then TimeNum=Year(t)*12*30+Month(t)*30+Day(t)
		If p=4 Then TimeNum=Year(t)*12+Month(t)
	End Function

	'-------------------------------------------------------------------------------------------
	'# AB.Time.TimeDiff(t1, t2, p)
	'# @return: Array (数组)
	'# @dowhat: 时间差计算函数 返回包含相差时间的数组, p值不同返回的数组不同。
	'--DESC------------------------------------------------------------------------------------
	'# @param t1: [string] (字符串) 时间1
	'# @param t2: [string] (字符串) 时间2
	'# @param p:  [integer] (整型) 显示格式 (默认值 0)
	'#  当 p = 0 时，返回一个 包含相差 x(秒) 的数组 (缺省)
	'#  当 p = 1 时，返回一个 包含相差 x(天)、x(小时)、x(分钟)、x(秒) 的数组
	'#  当 p = 2 时，返回一个 包含相差 x(年)、x(月) 的数组 （注：此算法仅供参考，有待验证!!）
	'--DEMO------------------------------------------------------------------------------------
	'# dim t1,t2
	'# t1 = CDate("2012-2-28 8:04:18")
	'# t2 = t1+28.52
	'# dim a,b,c
	'# a = ab.time.timediff(t1,t2,0) : b = ab.time.timediff(t1,t2,1) : c = ab.time.timediff(t1,t2,2)
	'# ab.c.printcn "时间1: " & t1
	'# ab.c.printcn "时间2: " & t2
	'# ab.c.printcn "时间相差: " & a(0)&"秒"
	'# ab.c.printcn "时间相差: " & b(0)&"天" & b(1)&"小时" & b(2)&"分钟" & b(3)&"秒"
	'# ab.c.printcn "时间相差: " & c(0)&"年" & c(1)&"月" '此算法仅供参考，有待验证!!
	'------------------------------------------------------------------------------------------

	Function TimeDiff(Byval t1, Byval t2, Byval p)
		Dim y,m,d,h,n,s,yy,mm,dd,hh,nn,ss
		y=DateDiff("yyyy",t1,t2) : m=DateDiff("m",t1,t2) : d=DateDiff("d",t1,t2)
		h=DateDiff("h",t1,t2) : n=DateDiff("n",t1,t2) : s=DateDiff("s",t1,t2)
		dd=Fix(s/3600/24)
		hh=Fix((s/3600-dd*24))
		nn=Fix((s/3600-dd*24)*60-hh*60)
		ss=Fix(((s/3600-dd*24)*60-hh*60-nn)*60)
		yy=y : If year(t1)=year(t2) Then yy=0 '如果日期同年，则年差为 0
		mm=m-y*12 : If m=0 Then mm=0
		If year(t1)=year(t2) and month(t1)=month(t2) Then mm=0 '如果同年同月，则月差为 0
		If m=1 and DateDiff("s",DateAdd("m",1,t1),CDate(t2))<0 Then mm=0
		If d<365 Then yy=0 '如果日差小于365天，则年差为 0
		If d<28 Then mm=0 '如果日差小于28天，则月差为 0
		Dim a()
		If p = "" Or isNull(p) Then p = 0 'p 默认为 0
		Select Case Lcase(p)
			Case "1" '返回相差：[日、时、分、秒] 的数组
				Redim a(3)
				a(0)=dd:a(1)=hh:a(2)=nn:a(3)=ss
			Case "2" '返回相差：[年、月] 的数组(此算法正不正确？有待验证)
				Redim a(2)
				a(0)=yy:a(1)=mm
			Case Else '返回相差：[秒] 的数组（缺省）
				Redim a(0)
				a(0)=s
		End Select
		TimeDiff = a
	End Function

	'====辅助函数====

		'补零
		Private Function FillZero(ByVal iValue)
			FillZero = Right("00"&iValue,2)
		End Function

		Private Function getMistiming(sDate)
			getMistiming = DateDiff("s", "1970-1-1 00:00:00", DateAdd("h", sTimeZone_, CDate(sDate)))
		End Function

		'获取设置的时区相关信息
		Private Function GetTimeZone__(Byval n)
			Dim z_ : z_ = sGMTTimeZone_
			Dim k_ : k_ = 1 : If Left(z_,1) = "-" Then k_ = -1
			z_ = Replace(Replace(z_,"-",""),"+","")
			Dim h_ : h_ = Abs(Cint(Split(z_,":")(0)))
			Dim m_ : m_ = Abs(Cint(Split(z_,":")(1)))
			Dim a(3)
			If k_=-1 Then a(0)= "-" & Right("00"&h_,2) & ":" & Right("00"&m_,2) Else a(0)= "+" & Right("00"&h_,2) & ":" & Right("00"&m_,2)
			If k_=-1 Then a(1)= "-" & Right("00"&h_,2) & "" & Right("00"&m_,2) Else a(1)= "+" & Right("00"&h_,2) & "" & Right("00"&m_,2)
			a(2) = k_ * (h_ + ((m_ * 0.1) / 60) * 10) 'zone
			a(3) = k_ * (60 * 60 * h_ + 60 * m_) '秒
			Dim temp
			Select Case n
				Case 0: temp = a(0)
				Case 1: temp = a(1)
				Case 2: temp = a(2)
				Case 3: temp = a(3)
			End Select
			GetTimeZone__ = temp
		End Function

		'根据时区获取GMTTimeZone
		Private Function ToGMTTimeZone__(Byval z)
			Dim k_ : k_ = 1 : If Left(Trim(""&z),1)="-1" Or z<0 Then k_ = -1
			Dim t_ : t_ = Abs(CDbl(z))
			Dim h_ : h_ = Abs(Fix(t_))
			Dim n_ : n_ = 0 : n_ = Abs(t_) - Abs(Fix(t_))
			Dim m_ : m_ = n_ * 60
			Dim s_ : s_ = "+" : If k_ = -1 Then s_ = "-"
			Dim temp
			temp = "" & s_ & Right("00" & h_, 2) & ":" & Right("00" & m_, 2)
			ToGMTTimeZone__ = temp
		End Function

		'检测服务端和客户端时区
		Private Function GetInfoTimeZone__(Byval n)
			AB.Use "Sc"
			Dim sc, jso
			Set sc = AB.Sc.New
			sc.Lang = "js"
			sc.Add "var localTimezone = (new Date().getTimezoneOffset()/60)*(-1);"
			sc.Add "var secondServer = " & DateDiff("s", "1970-01-01 08:00:00", Now()) & ";"
			sc.Add "var secondClient = parseInt(new Date().getTime()/1000);"
			sc.Add "var secondSub = secondServer - secondClient;"
			sc.Add "var time = new Date(); time.setTime(time.getTime() + secondSub*1000);"
			sc.Add "var serverTimezone = (time.getTimezoneOffset()/60)*(-1);"
			Set jso = sc.Object
			Dim a(1)
			a(0) = CInt(jso.serverTimezone)
			a(1) = CInt(jso.localTimezone)
			Set jso = Nothing
			Set sc = Nothing
			If IsEmpty(n) Or Not IsNumeric(n) Then n = 0 Else n = CInt(n)
			If n = 1 Then GetInfoTimeZone__ = a(1) Else GetInfoTimeZone__ = a(0)
		End Function

End Class
%>