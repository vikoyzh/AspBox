<%
'######################################################################
'## ab.form.asp
'## -------------------------------------------------------------------
'## Feature     :   AspBox Form Class
'## Version     :   v1.0.1
'## Author      :   Lajox(lajox@19www.com)
'## Update Date :   2014/3/26 12:38
'## Description :   AspBox表单(Form)处理模块
'## 				注意： AB.Form.Init() 方法必须提前调用!!!
'######################################################################

Class Cls_AB_Form

	Public oForm, oFile
	Private o_item, o_file
	Private b_init
	Private s_fun, s_up
	
	Private Sub Class_Initialize
		b_init = False
		Set oForm = Server.CreateObject(AB.DictName)
		Set oFile = Server.CreateObject(AB.DictName)
		Set o_item = Server.CreateObject(AB.DictName)
		Set o_file = Server.CreateObject(AB.DictName)
		s_fun = ""
		s_up = ""
	End Sub

	Private Sub Class_Terminate
		IF Request.TotalBytes>0 then
			oForm.RemoveAll
			oFile.RemoveAll
			Set oForm = Nothing
			Set oFile = Nothing
			Set o_item  = Nothing
			Set o_file  = Nothing
		End IF
	End Sub

	'------------------------------------------------------------------------------------------
	'# AB.Form.Init()
	'# @return: void
	'# @dowhat: 初始化执行(要获取表单元素此方法必须提前调用!!!)
	'--DESC------------------------------------------------------------------------------------
	'# @param : none
	'--DEMO------------------------------------------------------------------------------------
	'@ ab.form.init() : if ab.form.count>0 then ab.c.print "表单元素共有：" & ab.form.count
	'------------------------------------------------------------------------------------------

	Public Sub Init()
		If b_init = False Then
			If FormType="file" Then
				If Trim(s_up)="" Then GetFileData()
			End If
		End If
		b_init = True
	End Sub

	Public Property Get objForm()
		Init()
		Select Case LCase(s_up)
			Case "upload"
				Set objForm = AB.Upload.Form
			Case "up.an"
				Set objForm = AB.Up.An.Forms("-1")
			Case "up.u5x"
				Set objForm = AB.Up.u5x.objForm
			Case "up.upfile"
				Set objForm = AB.Up.Upfile.Form
			Case Else
				Set objForm = oForm
		End Select
	End Property

	Public Property Get objFile()
		Init()
		Select Case LCase(s_up)
			Case "upload"
				Set objFile = AB.Upload.File
			Case "up.an"
				Set objFile = AB.Up.An.Files("-1")
			Case "up.u5x"
				Set objFile = AB.Up.u5x.objFile
			Case "up.upfile"
				Set objFile = AB.Up.Upfile.File
			Case Else
				Set objFile = oFile
		End Select
	End Property

	Public Property Get Method()
		Method = Request.ServerVariables("REQUEST_METHOD")
	End Property

	Public Property Get IsPost()
		IsPost = (Method = "POST")
	End Property

	Public Property Get IsGet()
		IsGet = (Method = "GET")
	End Property

	'------------------------------------------------------------------------------------------
	'# AB.Form.Up 属性
	'# @syntax: AB.Form.Up[ = f]
	'# @return: String (字符串)
	'# @dowhat: '设置表单上传形式
	'--DESC------------------------------------------------------------------------------------
	'# @param: f(可选) String (字符串)
	'# 可选，设置表单上传形式，值有：空值, upload, up.an, up.u5x, up.upfile
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Form.Up = "upload"
	'------------------------------------------------------------------------------------------

	Public Property Let Up(ByVal f)
		s_up = LCase(f)
	End Property
	Public Property Get Up()
		Up = s_up
	End Property

	'------------------------------------------------------------------------------------------
	'# AB.Form.Fun = f
	'# @return: void
	'# @dowhat: 对Form表单元素执行函数后结果
	'--DESC------------------------------------------------------------------------------------
	'# @param s : string|array 设置的函数（一般指定为全局函数，可以具体对象的公有方法 如 AB.E.UnEscape）
	'--DEMO------------------------------------------------------------------------------------
	'@ ab.form.fun = "f1"
	'@ ab.form.fun = "f1|f2|f3" '多个函数用(,)或(|)符号隔开
	'@ ab.form.fun = "AB.E.UnEscape"
	'@ ab.form.fun = "AB.E.UnEscape|Len"
	'------------------------------------------------------------------------------------------

	Public Property Let Fun(ByVal f)
		s_fun = f
	End Property

	'------------------------------------------------------------------------------------------
	'# AB.Form.GetFun()
	'# @return: Array 数组
	'# @dowhat: 取得设置函数列表
	'--DESC------------------------------------------------------------------------------------
	'# @param : none
	'--DEMO------------------------------------------------------------------------------------
	'@ none
	'------------------------------------------------------------------------------------------

	Public Property Get GetFun()
		Dim i, p, ary : ary = Array()
		If IsArray(s_fun) Then
			p = s_fun
		Else
			s_fun = Replace(s_fun, ",", "|")
			p = Split(Trim(s_fun), "|")
		End If
		For Each i In p
			If Trim(i)<>"" Then ary = AB.A.Push(ary, Trim(i))
		Next
		GetFun = ary
	End Property

	'------------------------------------------------------------------------------------------
	'# AB.Form.Count
	'# @alias: AB.Form.Length
	'# @return: integer
	'# @dowhat: 获取表单元素个数(只读)
	'--DESC------------------------------------------------------------------------------------
	'# @param : none
	'--DEMO------------------------------------------------------------------------------------
	'@ ab.form.init() : if ab.form.count>0 then ab.c.print "表单元素共有：" & ab.form.count
	'------------------------------------------------------------------------------------------

	Public Property Get Count()
		Dim n:n=0
		IF FormType="file" Then
			Init()
			If objForm.Count>0 Then n=objForm.Count
		ElseIF FormType="text" Then
			n=Request.Form.Count
		End If
		Count = n
	End Property
	Public Property Get Length() : Length = Me.Count() : End Property

	'----------------------------------------------------------------------------------------------
	'# AB.Form.Get(p) 简写成 AB.Form(p)
	'# @return: (String | Integer) | Array
	'# @dowhat: 用于获取表单元素项值，可替代常规的Request.Form,且支持multipart/form-data类型表单
	'--DESC----------------------------------------------------------------------------------------
	'# @param : p参数说明:
	'#    当参数p值为空或为"-1"时候，返回所有表单元素数组 e.g. AB.C.Print IsArray(AB.Form.Get(-1))
	'#    当参数p为字符串类型，则获取表单元素名称为p的值 e.g. AB.C.Print AB.Form.Get("form_title"))
	'#    当参数p为int型，如果p>=0则获取表单元素数组索引为p的值 e.g. ab.c.print ab.form.get(0)
	'--DEMO----------------------------------------------------------------------------------------
	'# 调用示例:
	'# ab.form.init()
	'# dim i
	'# if ab.form.count>0 then
	'# 	for i=1 to ab.form.count
	'#	 	ab.c.print ab.form(i) & "<br>"
	'# 	next
	'# end if
	'# if isarray(ab.form.get(-1)) then
	'#	for i=0 to ubound(ab.form.get(-1))
	'#	 	ab.c.print ab.form.get(i) & "<br>"
	'# 	next
	'# end if
	'----------------------------------------------------------------------------------------------

	Public Default Function [Get](Byval p)
		On Error Resume Next
		Dim s,i,k,a()
		If IsNull(p) Or Trim(p)="" Or Cstr(p)="-1" Or AB.C.IsNum(p) Then
			k = 0
			IF FormType="file" Then
				Init()
				For Each i In objForm
					Redim Preserve a(k)
					''a(k) = oForm(i)
					Set a(k) = Node(i)
					k = k + 1
				Next
			ElseIF FormType="text" Then
				For Each i In Request.Form
					Redim Preserve a(k)
					''a(k) = Request.Form(i)
					Set a(k) = Node(i)
					k = k + 1
				Next
			End If
			Dim n:n=Ubound(a)+1 : If Err Then n = 0
			''If n>0 Then If AB.C.IsNum(p) and Cstr(p)<>"-1" Then s = a(Cint(p)) Else  s = a
			''s = FunSet(s) '应用fun
			If n>0 Then If AB.C.IsNum(p) and Cstr(p)<>"-1" Then Set s = a(Cint(p)) Else  s = a
		Else
			''s = FormVar(p)
			Set s = Node(p)
		End If
		''[Get] = s
		Set [Get] = s
		On Error Goto 0
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.Form.Items()
	'# @return: void
	'# @dowhat: 获取表单提交的所有元素字典(只读)
	'--DESC------------------------------------------------------------------------------------
	'# @param : none
	'--DEMO------------------------------------------------------------------------------------
	'@ Dim element
	'@ IF AB.Form.Items.Count<>0 Then
	'@ 	For Each element In AB.Form.Items
	'@ 		AB.C.PrintCn "AB.Form.Item("""&element&""") = " & AB.Form.Item(element)
	'@ 	Next
	'@ End IF
	'------------------------------------------------------------------------------------------

	Public Property Get Items()
		'If IsObject(objForm) Then Set Items = objForm Else Set Items = Nothing
		Dim element,i,temp
		IF FormType="file" Then
			Init()
			IF objForm.Count<>0 Then
				For Each element In objForm
					o_item(element) = objForm.Item(element)
					o_item(element) = FunSet(o_item(element)) '应用fun
				Next
			End IF
		ElseIF FormType="text" Then
			For each element In Request.Form
				temp = array()
				For i=1 To Request.Form(element).Count
					Redim Preserve temp(i-1)
					temp(i-1) = Request.Form(element)(i)
				Next
				If UBound(temp)>0 Then temp = Join(temp, ", ")
				If Request.Form(element).Count <= 1 Then temp = Request.Form(element)
				o_item(element) = temp
				o_item(element) = FunSet(o_item(element)) '应用fun
			Next
		End If
		Set Items = o_item
	End Property

	Public Property Get Item(Byval e)
		Dim oItems : Set oItems = Items()
		If oItems.Exists(e) Then
			If IsObject(oItems(e)) Then Set Item = oItems(e) Else Item = oItems(e)
		Else
			Item = Empty
		End If
	End Property

	'------------------------------------------------------------------------------------------
	'# AB.Form.Has
	'# @return: boolean
	'# @dowhat: 检测是否含有某个表单元素
	'--DESC------------------------------------------------------------------------------------
	'# @param : e 表单元素项
	'--DEMO------------------------------------------------------------------------------------
	'@ If ab.form.has("username") then ab.c.print ab.form("username")
	'------------------------------------------------------------------------------------------

	Public Property Get Has(Byval e)
		Has = False
		Dim oItems : Set oItems = Items()
		If oItems.Exists(e) Then
			Has = True
		End If
	End Property

	'------------------------------------------------------------------------------------------
	'# AB.Form.Files()
	'# @return: void
	'# @dowhat: 获取表单提交的所有file字典(只读)
	'--DESC------------------------------------------------------------------------------------
	'# @param : none
	'--DEMO------------------------------------------------------------------------------------
	'@ Dim element
	'@ IF AB.Form.Files.Count<>0 Then
	'@ 	For Each element In AB.Form.Files
	'@ 		AB.C.PrintCn "AB.Form.File("""&element&""") : "
	'@ 		AB.C.PrintCn "AB.Form.File("""&element&""").FormName = " & AB.Form.File(element).FormName
	'@ 		AB.C.PrintCn "AB.Form.File("""&element&""").FileName = " & AB.Form.File(element).FileName
	'@ 		AB.C.PrintCn "AB.Form.File("""&element&""").FilePath = " & AB.Form.File(element).FilePath
	'@ 		AB.C.PrintCn "AB.Form.File("""&element&""").FileSize = " & AB.Form.File(element).FileSize
	'@ 		AB.C.PrintCn "AB.Form.File("""&element&""").FileType = " & AB.Form.File(element).FileType
	'@ 		AB.C.PrintCn "------"
	'@ 	Next
	'@ End IF
	'------------------------------------------------------------------------------------------

	Public Property Get Files()
		Dim element
		IF FormType="file" Then
			Init()
			IF objFile.Count<>0 Then
				For Each element In objFile
					If IsObject(objFile.Item(element)) Then Set o_file(element) = objFile.Item(element) Else o_file(element) = objFile.Item(element)
				Next
			End IF
		End If
		'If IsObject(objFile) Then Set Files = objFile Else Set Files = Nothing
		Set Files = objFile
	End Property

	Public Property Get File(ByVal s)
		s = Lcase(s)
		IF Not objFile.exists(s) Then
		  Set File = New abForm_FileInfo_Class
		Else
		  Set File = objFile.Item(s)
		End IF
	End Property

	'------------------------------------------------------------------------------------------
	'# AB.Form.FormEncType()
	'# @return: string
	'# @dowhat: 获取表单EncType的值
	'--DESC------------------------------------------------------------------------------------
	'# @param : none
	'--DEMO------------------------------------------------------------------------------------
	'@ AB.C.Print AB.Form.FormEncType
	'------------------------------------------------------------------------------------------

	Public Function FormEncType()
		Dim sOutput, sContentType
		sContentType = Request.ServerVariables("HTTP_CONTENT_TYPE")
		If Trim(sContentType)="" Then sContentType = Request.ServerVariables("CONTENT_TYPE")
		IF Instr(sContentType,"multipart/form-data")>0 Then sOutput = "multipart/form-data"
		IF Instr(sContentType,"application/x-www-form-urlencoded")>0 Then sOutput = "application/x-www-form-urlencoded"
		IF Instr(sContentType,"text/plain")>0 Then sOutput = "text/plain"
		IF sOutput="" Then sOutput = sContentType
		FormEncType = sOutput
	End Function
	Public Function EncType() : EncType = FormEncType() : End Function

	'------------------------------------------------------------------------------------------
	'# AB.Form.FormType()
	'# @alias: AB.Form.Type()
	'# @return: string
	'# @dowhat: 获取表单类型,返回值为file或text
	'--DESC------------------------------------------------------------------------------------
	'# @param : none
	'--DEMO------------------------------------------------------------------------------------
	'@ AB.C.Print AB.Form.FormType
	'------------------------------------------------------------------------------------------

	Public Function FormType()
		Dim sOutput, sContentType
		sContentType = FormEncType
		IF Instr(sContentType,"multipart/form-data")>0 Then sOutput = "file"
		IF Instr(sContentType,"application/x-www-form-urlencoded")>0 Then sOutput = "text"
		IF Instr(sContentType,"text/plain")>0 Then sOutput = "other"
		FormType = sOutput
	End Function
	Public Function [Type]() : [Type] = FormType() : End Function

	'------------------------------------------------------------------------------------------
	'# AB.Form.FormVar(s)
	'# @alias: AB.Form.Var(s)
	'# @return: string | integer
	'# @dowhat: 获取表单元素值
	'--DESC------------------------------------------------------------------------------------
	'# @param s: [string] (字符串)
	'--DEMO------------------------------------------------------------------------------------
	'@ AB.C.Print AB.Form.FormVar("form_name")
	'------------------------------------------------------------------------------------------

	Public Function FormVar(ByVal s)
		Dim p:p=s
		' If False Then
			' IF FormType="file" Then
				' Init()
				' p = Lcase(p)
				' IF Not objForm.exists(p) then
					' FormVar = ""
				' Else
					' FormVar = objForm.Item(p)
				' End IF
			' ElseIF FormType="text" Then
				' FormVar = Request.Form(p)
			' End IF
			' FormVar = FunSet(FormVar) '应用fun
		' End If
		Set FormVar = Node(p)
	End Function
	Public Function Var(ByVal s) : Set Var = FormVar(s) : End Function

	Public Function Node(Byval p)
		Dim o_key : Set o_key = New Cls_Form_Key
		o_key.Dom = Me
		o_key.Name = p
		o_key.Type = FormType
		Set Node = o_key
	End Function

	'-----------------------------------------------------------------------------------------------
	'# AB.Form.arrForm(s)
	'# @return: array
	'# @dowhat: 批量获取表单元素值并存为数组, 当参数s值为空时，返回所有表单元素数组
	'# 			若参数s以"exp:"为前缀开头则表示获取除了指定的元素之外所有元素存为数组
	'--DESC-----------------------------------------------------------------------------------------
	'# @param s: [string] (字符串)
	'--DEMO-----------------------------------------------------------------------------------------
	'# if isarray(ab.form.arrformvar("")) then ab.c.print ubound(ab.form.arrformvar(""))
	'# dim arrForm : arrForm = ab.form.arrform("form_title,form_content") : if isarray(arrform) then ab.c.print arrform(0)
	'# dim arrForm : arrForm = ab.form.arrform("exp:form_item1,form_item2") : if isarray(arrform) then ab.c.print arrform(0)
	'-----------------------------------------------------------------------------------------------

	Public Function arrForm(Byval s)
		On Error Resume Next
		Dim a(), b, i, j, k, temp
		If Trim(s)<>"" Then
			If Instr(s,":")>0 Then
				IF Lcase(Left(s,4))="exp:" Then
					s = Trim(Right(s,Len(s)-4))
					b = Split(s,",")
					Dim bIfHas : bIfHas = False
					k = 0
					For Each i In objForm
						bIfHas = False
						For j=0 to Ubound(b)
							If Trim(b(j))<>"" and Trim(b(j))=i then
								bIfHas = True
								Exit For
							End If
						Next
						If bIfHas = False Then
							Redim Preserve a(k)
							a(k) = objForm.Item(i)
							k = k + 1
						End If
					Next
				End If
				a = FunSet(a) '应用fun
			Else
				b = Split(s,",")
				k = 0
				For i=0 to Ubound(b)
					If Trim(b(i))<>"" Then
						Redim Preserve a(i)
						a(k) = FormVar(Trim(b(i)))
						k = k + 1
					End If
				Next
			End If
		Else
			k = 0
			For Each i In objForm
				Redim Preserve a(k)
				a(k) = objForm.Item(i)
				k = k + 1
			Next
			a = FunSet(a) '应用fun
		End If
		Dim n:n=Ubound(a)+1 : If Err Then n = 0
		If n>0 Then temp = a
		arrForm = a
		On Error Goto 0
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.Form.QueryForm(s)
	'# @return: array
	'# @dowhat: 将指定的表单元素存储为数组，每个用(,)隔开
	'--DESC------------------------------------------------------------------------------------
	'# @param s: [string] (字符串) 指定的表单元素，每个用(,)隔开
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Trace AB.Form.QueryForm("name,title,artist")(0)
	'------------------------------------------------------------------------------------------

	Function QueryForm(oStr)
		On Error Resume Next
		Dim A(),I,K:K=0
		IF IsNull(oStr) Or Trim(oStr)="" Then:Exit Function:End IF
		oStr = AB.C.RP(oStr, "|", ",")
		For I=0 To Ubound(Split(oStr,","))
			Redim Preserve A(K)
			IF Trim(Split(oStr,",")(I))<>"" Then
				'A(K) = Request.Form(Cstr(Trim(Split(oStr,",")(I))))
				A(K) = FormVar(Cstr(Trim(Split(oStr,",")(I))))
				K=K+1
			End IF
		Next
		QueryForm = A
		On Error Goto 0
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.Form.ShowForms()
	'# @return: array
	'# @dowhat: 输出全部表单元素(排除文件元素)
	'--DESC------------------------------------------------------------------------------------
	'# @param : none
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Form.ShowForms()
	'------------------------------------------------------------------------------------------

	Public Sub ShowForms()
		On Error Resume Next
		Dim element,i,oStr,temp
		IF FormType="file" Then
			Init()
			IF objForm.Count<>0 Then
				For Each element In objForm
					objForm.Item(element) = FunSet(objForm.Item(element)) '应用fun
					oStr = AB.C.IIF(AB.C.IsInt(objForm.Item(element)), objForm.Item(element), """" & objForm.Item(element) & """")
					AB.C.Print "Request.Form("""& element &""")" & " = " & oStr & "<br />" & VBCrlf
				Next
			End IF
		ElseIF FormType="text" Then
			For each element In Request.Form
				temp = array()
				For i=1 To Request.Form(element).Count
					Redim Preserve temp(i-1)
					temp(i-1) = Request.Form(element)(i)
					temp(i-1) = FunSet(temp(i-1)) '应用fun
				Next
				temp = Join(temp, ", ")
				oStr = AB.C.IIF(AB.C.IsInt(temp), temp, """" & temp & """")
				AB.C.Print "Request.Form("""& element &""")" & " = " & oStr & "<br />" & VBCrlf
			Next
		End If
		On Error Goto 0
	End Sub

	'------------------------------------------------------------------------------------------
	'# AB.Form.ShowFiles()
	'# @return: array
	'# @dowhat: 输出全部文件元素
	'--DESC------------------------------------------------------------------------------------
	'# @param : none
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Form.ShowFiles()
	'------------------------------------------------------------------------------------------

	Public Sub ShowFiles()
		On Error Resume Next
		Dim element,i,oStr
		IF FormType="file" Then
			Init()
			IF objFile.Count<>0 Then
				For Each element In objFile
					oStr=""
					AB.C.Print "AB.Form.File("""& element &""")" & " => {" & "<br />" & VBCrlf
					AB.C.Print "&nbsp;AB.Form.File("""& element &""").FormName" & " = """ & objFile.Item(element).FormName & """<br />" & VBCrlf
					AB.C.Print "&nbsp;AB.Form.File("""& element &""").FileName" & " = """ & objFile.Item(element).FileName & """<br />" & VBCrlf
					AB.C.Print "&nbsp;AB.Form.File("""& element &""").FilePath" & " = """ & objFile.Item(element).FilePath & """<br />" & VBCrlf
					AB.C.Print "&nbsp;AB.Form.File("""& element &""").FileSize" & " = " & objFile.Item(element).FileSize & "<br />" & VBCrlf
					AB.C.Print "&nbsp;AB.Form.File("""& element &""").FileType" & " = """ & objFile.Item(element).FileType & """<br />" & VBCrlf
					AB.C.Print  "}" & "<br />" & VBCrlf
				Next
			End IF
		End If
		On Error Goto 0
	End Sub

	'-----------------------------------------------------------------------------------------------
	'# AB.Form.FormValid(s, sm, sn)
	'# @return: array
	'# @dowhat: 批量对指定的表单元素进行表单验证
	'# 			支持以下数据类型验证：int;require;date;email;logic;enum-int;group;password;float
	'# 			float小数类型验证，默认为2位
	'# 			支持multipart-data类型的表单
	'--DESC-----------------------------------------------------------------------------------------
	'# @param s: [string] (字符串) 指定表单元素及类型
	'# @param sm: [string] (字符串) 设置每个表单元素类型分隔符
	'# @param sn: [string] (字符串) 设置每个表单元素项分隔符
	'--DEMO-----------------------------------------------------------------------------------------
	'# AB.Form.FormValid "form_title:string:标题不能为空;form_cat:int:类别未选择;form_date>11:logic:日期不得大于10天;
	'#              			form_pwd<>form_pwd2:logic:两次密码输入不一致;form_email:email:电子邮件格式错误;
	'#              			from_sex:group:性别未选择",":",";"
	'# AB.Form.FormValid "form_ranking:enum-int:名次只能是数字;form_vcode<>lcase(session(""GetCode"")):logic:验证码不正确",":",";"
	'# AB.Form.FormValid "mydate:float:0,工作周期不能为小数;myprice:float:2,价格最多只能包含两位小数",":",";"
	'-----------------------------------------------------------------------------------------------

	Public Function FormValid(Byval s, Byval sm, Byval sn)
		IF Instr(s, sm)<1 Then Exit Function
		Dim sValidMode : sValidMode = "javascript" '验证返回方式
		Dim n, msg, arr, item, arrItem, oReg
		Dim sField, sDataType, sErrTip, sValue, sBR, iBitLen
		sBR = "\n" : n=0 : msg="" : arr=split(s, sn)
		For Each item In arr
			IF item<>"" Then
				arrItem = split(item, sm)	
				IF Ubound(arrItem)=2 Then
					sField = arrItem(0)
					sDataType = arrItem(1)
					sErrTip = arrItem(2)
					IF sDataType<>"logic" Then sValue=FormVar(sField)
					Select Case sDataType
						Case "int":								
							IF IsNumeric(sValue)=false Then
								n=n+1
								msg=msg & sErrTip & sBR
							End IF
					    Case "string":
					        sValue=Ltrim(Rtrim(sValue))
					        IF sValue="" Then
					            n=n+1
					            msg=msg & sErrTip & sBR
					        End IF
						Case "date":
						    IF isdate(sValue)=false Then
							    n=n+1
							    msg=msg & sErrTip & sBR
						    End IF							
						Case "password":
							IF trim(sValue)<>"" Then								
								set oReg=new Regexp
								oReg.pattern="^[a-zA-Z\d_]+$"
								IF(oReg.Test(sValue)=False) Then
									n=n+1
								    msg=msg & sErrTip & sBR
								End IF
								Set oReg=nothing
							End IF
						Case "group":
						    IF trim(replace(sValue,",",""))="" Then 
						        n=n+1
								msg=msg & sErrTip & sBR
						    End IF
						Case "email":
						    IF sValue<>"" Then
							    IF Instr(sValue,"@")<1 or Instr(sValue,".")<1 Then
								    n=n+1
								    msg=msg & sErrTip & sBR
							    End IF
							End IF
					    Case "enum-int":
					        For Each cls_int_value in split(sValue,",")
					            IF IsNumeric(cls_int_value)=False or cls_int_value="" Then 
								    n=n+1
								    msg=msg & sErrTip & sBR					            
					            End IF
					        Next
					    Case "float":
					        IF IsNumeric(sValue) and sValue<>"" Then
					            iBitLen=left(sErrTip,Instr(sErrTip,",")-1)
					            IF IsNumeric(iBitLen)=False or iBitLen="" Then iBitLen=2
					            IF Instr(sValue,".")>0 Then	
					                IF len(mid(sValue,Instr(sValue,".")+1))>abs(iBitLen) Then
					                    n=n+1
								        msg=msg & mid(sErrTip,Instr(sErrTip,",")+1) & sBR		 
					                End IF
					            End IF
					        Else
					            n=n+1
								msg=msg & sField & "不是有效的数值！" & sBR		
					        End IF
					    Case "logic":			
					       dim arrLogic,strValue1,strValue2
					       IF Instr(sField,">")>0 and Instr(sField,"<")=0 Then
					            arrLogic=split(sField,">")
					            strValue1=FormVar(arrLogic(0))
					            strValue2=FormVar(arrLogic(1))					            
					            IF strValue2="" Then strValue2=eval(arrLogic(1))
					            IF IsNumeric(strValue1) and IsNumeric(strValue2) Then			            
					                IF round(strValue1)>round(strValue2) Then
					                    n=n+1
					                    msg=msg & sErrTip & sBR
					                End IF
					            End IF
					       End IF
						   IF Instr(sField,"<")>0 and Instr(sField,">")=0 Then
					            arrLogic=split(sField,"<")
					            strValue1=FormVar(arrLogic(0))
					            strValue2=FormVar(arrLogic(1))											            
					            IF strValue2="" Then strValue2=eval(arrLogic(1))								
					            IF IsNumeric(strValue1) and IsNumeric(strValue2) Then			            
					                IF round(strValue1)<round(strValue2) Then
					                    n=n+1
					                    msg=msg & sErrTip & sBR
					                End IF
					            End IF
					       End IF
						   IF Instr(sField,"=")>0 Then
					            arrLogic=split(sField,"=")
					            strValue1=FormVar(arrLogic(0))
					            strValue2=FormVar(arrLogic(1))					            
					            IF strValue2="" Then strValue2=eval(arrLogic(1))
					            IF strValue1=strValue2 Then
					                n=n+1
					                msg=msg & sErrTip & sBR
					            End IF	
					       End IF
						   IF Instr(sField,"<>")>0 Then							  				   
					            arrLogic=split(sField,"<>")
					            strValue1=FormVar(arrLogic(0))
					            strValue2=FormVar(arrLogic(1))					            
					            IF strValue2="" Then strValue2=eval(arrLogic(1))								
					            IF strValue1<>strValue2 Then
					                n=n+1
					                msg=msg & sErrTip & sBR
					            End IF					       			       
					       End IF					        
						Case else:
							IF len(sValue)=0 Then
								n=n+1
								msg=msg & sErrTip & sBR
							End IF							
					End Select
				End IF
			End IF
		next
		IF n>0 Then
		    Select Case sValidMode
		        Case "javascript":
			        AB.C.Js "alert('" & msg & "');history.go(-1);"
			    Case "text":
			        FormValid = Replace(msg, sBR, "<br />")
			    Case "json":
			        FormValid = "{'err':'" & msg & "'}"
			End Select
		End IF		
	End Function

	Public Function FunSet(ByVal a)
		On Error Resume Next
		Dim i, f : f = GetFun()
		If Not AB.C.isNul(f) Then
			If IsArray(a) Then
				If Not AB.C.isNul(a) Then
					For i=0 To UBound(a)
						a(i) = FunSet(a(i))
					Next
				End If
			Else
				For Each i In f
					Execute("a = "& i &"(a)")
					If Err.Number<>0 Then
						AB.Error.Msg = "(执行函数 "& i &" 出错)"
						AB.Error.Raise 1
					End If
				Next
			End If
		End If
		FunSet = a
		On Error Goto 0
	End Function

	' -------------- 以下辅助函数 -------------------

		Private Sub GetFileData()
			On Error Resume Next
			IF Err Then Err.Clear
			dim RequestData,sStart,vbCrlf,sInfo,iInfoStart,iInfoEnd,tStream,oStream,iStart,theFile
			dim iFileSize,sFilePath,sFileType,sFormValue,sFileName
			dim iFindStart,iFindEnd
			dim iFormStart,iFormEnd,sFormName
			IF Request.TotalBytes<1 Then Exit Sub
			Set tStream = Server.CreateObject(AB.steamName)
			Set oStream = Server.CreateObject(AB.steamName)
			oStream.Type = 1
			oStream.Mode = 3
			oStream.Open
			oStream.Write Request.BinaryRead(Request.TotalBytes)
			oStream.Position = 0
			RequestData = oStream.Read
			iFormStart = 1
			iFormEnd = LenB(RequestData)
			vbCrlf = chrB(13) & chrB(10)
			sStart = MidB(RequestData, 1, InStrB(iFormStart,RequestData,vbCrlf)-1)
			iStart = LenB(sStart)
			iFormStart = iFormStart + iStart + 1
			While (iFormStart + 10) < iFormEnd
				iInfoEnd = InStrB(iFormStart,RequestData,vbCrlf & vbCrlf)+3
				tStream.Type = 1
				tStream.Mode =3
				tStream.Open
				oStream.Position = iFormStart
				oStream.CopyTo tStream,iInfoEnd-iFormStart
				tStream.Position = 0
				tStream.Type = 2
				tStream.Charset = AB.CharSet
				sInfo = tStream.ReadText
				tStream.Close
				'取得表单项目名称
				iFormStart = InStrB(iInfoEnd,RequestData,sStart)
				iFindStart = InStr(22,sInfo,"name=""",1)+6
				iFindEnd = InStr(iFindStart,sInfo,"""",1)
				sFormName = Lcase(Mid(sinfo,iFindStart,iFindEnd-iFindStart))
				'如果是文件
				IF InStr(45,sInfo,"filename=""",1) > 0 then
					Set theFile = New abForm_FileInfo_Class
					'取得文件名
					iFindStart = InStr(iFindEnd,sInfo,"filename=""",1)+10
					iFindEnd = InStr(iFindStart,sInfo,"""",1)
					sFileName = Mid(sinfo,iFindStart,iFindEnd-iFindStart)
					theFile.FileName = getFileName(sFileName)
					theFile.FilePath = getFilePath(sFileName)
					'取得文件类型
					iFindStart = InStr(iFindEnd,sInfo,"Content-Type: ",1)+14
					iFindEnd = InStr(iFindStart,sInfo,vbCr)
					theFile.FileType = Mid(sinfo,iFindStart,iFindEnd-iFindStart)
					theFile.FileStart = iInfoEnd
					theFile.FileSize = iFormStart -iInfoEnd -3
					theFile.FormName = sFormName
					IF Not oFile.Exists(sFormName) Then oFile.add sFormName,theFile
				Else
					'如果是表单项目
					tStream.Type =1
					tStream.Mode =3
					tStream.Open
					oStream.Position = iInfoEnd
					oStream.CopyTo tStream,iFormStart-iInfoEnd-3
					tStream.Position = 0
					tStream.Type = 2
					tStream.Charset = AB.CharSet
					sFormValue = tStream.ReadText
					tStream.Close
					'---
					IF oForm.Exists(sFormName) then
						oForm(sFormName)=oForm(sFormName)&", "&sFormValue
					Else
						oForm.Add sFormName,sFormValue
					End IF
				End IF
				iFormStart=iFormStart+iStart+1
			wend
			RequestData=""
			tStream.Close : Set tStream =nothing
			oStream.Close : Set oStream =nothing
			On Error Goto 0
		End Sub

		Private Function GetFilePath(Byval FullPath)
			If Trim(FullPath) <> "" Then
				GetFilePath = left(FullPath,InStrRev(FullPath, "\"))
			Else
				GetFilePath = ""
			End If
		End Function

		Private Function GetFileName(Byval FullPath)
			If Trim(FullPath) <> "" Then
				GetFileName = Mid(FullPath,InStrRev(FullPath, "\")+1)
			Else
				GetFileName = ""
			End If
		End Function

End Class

Class Cls_Form_Key
	Private s_key, s_type, o_dom
	Private Sub Class_Initialize() : AB.Use "Form" : Set o_dom = AB.Form : End Sub
	Private Sub Class_Terminate() : Set o_dom = Nothing : End Sub
	Public Property Let Name(ByVal p) : s_key = p : End Property
	Public Property Get Name() : Name = s_key : End Property
	Public Property Let Dom(ByVal o) : Set o_dom = o : End Property
	Public Property Get Dom() : Set Dom = o_dom : End Property
	Public Property Let [Type](ByVal p) : s_type = p : End Property
	Public Property Get [Type]() : s_type = AB.C.NoneIs("text", s_type) : [Type] = s_type : End Property
	Public Default Function Value() : Value = Item(s_key) : End Function
	Public Property Get Item(Byval p)
		s_type = AB.C.NoneIs("text", s_type)
		If s_type="file" Then
			o_dom.Init()
			p = Lcase(p)
			IF Not o_dom.objForm.Exists(p) then
				Item = ""
			Else
				Item = o_dom.objForm.Item(p)
			End IF
		Else
			Item = Request.Form(p)
		End If
		Item = o_dom.FunSet(Item) '应用fun
	End Property
End Class

Class abForm_FileInfo_Class
	Dim FormName,FileName,FilePath,FileSize,FileType,FileStart
End Class
%>