<%
'######################################################################
'## ctrl.make.asp
'## -------------------------------------------------------------------
'## Feature     :   AspBox Mvc-Ctrl Make Class
'## Version     :   v1.0
'## Author      :   Lajox(lajox@19www.com)
'## Update Date :   2012/04/29 23:50
'## Description :   AspBox MVC模型之Ctrl层之(Make)创建生成模块核心类
'######################################################################

Class Cls_Ctrl_Make

	Private Sub Class_Initialize()

	End Sub

	Private Sub Class_Terminate()

	End Sub

	'------------------------------------------------------------------------------
	'# Ctrl.Make.CreatDB_MsAcc(DbPath, DbFileName, DbUpwd)
	'# @return: boolean
	'# @dowhat: 创建MsAccess数据库(已存在则覆盖)
	'# 			创建成功返回 True 失败 False
	'--DESC------------------------------------------------------------------------
	'# @param DbPath 		[string] : 目标目录信息
	'# @param DbFileName 	[string] : 目标库文件名称
	'# @param DbUpwd 		[string] : 目标库打开密码
	'--DEMO------------------------------------------------------------------------
	'# Dim isMaked : isMaked = False
	'# AB.Use "Mvc" : Ctrl.Use "Make" : isMaked = Ctrl.Make.CreatDB_MsAcc("E:\", "test.mdb", "")
	'# If isMaked Then AB.C.Print "成功创建Access数据库" Else AB.C.Print "创建失败"
	'#-----------------------------------------------------------------------------

	Public Function CreatDB_MsAcc(ByVal DbPath,ByVal DbFileName,ByVal DbUpwd)
		CreatDB_MsAcc = False
		On Error GoTo 0
		On Error Resume Next
		AB.Use "Mvc" : Dao.Use "Work"
		CreatDB_MsAcc = Dao.Work.CreatDB_MsAcc(DbPath, DbFileName, DbUpwd)
		On Error GoTo 0
	End function

	'------------------------------------------------------------------------------
	'# Ctrl.Make.CreatDB_MsSQL(DbIp, DbSamc, DbSapwd, DbName, DbUpmc, DbUpwd)
	'# @return: boolean
	'# @dowhat: 创建MsSQLServer数据库
	'# 			创建成功返回 True 失败 False
	'--DESC------------------------------------------------------------------------
	'# @param DbIp 			[string] : 数据库所在IP或主机名称
	'# @param DbSamc 		[string] : 数据库超管用户名称
	'# @param DbSapwd 		[string] : 数据库超管用户口令
	'# @param DbName 		[string] : 新建数据库名称
	'# @param DbSapwd 		[string] : 新建数据库所属用户名称
	'# @param DbUpmc 		[string] : 新建数据库所属用户密码
	'--DEMO------------------------------------------------------------------------
	'#
	'#-----------------------------------------------------------------------------

	Public Function CreatDB_MsSQL(ByVal DbIp,ByVal DbSamc,ByVal DbSapwd,ByVal DbName,ByVal DbUpmc,ByVal DbUpwd)
		CreatDB_MsSQL = False
		On Error GoTo 0
		On Error Resume Next
		AB.Use "Mvc" : Dao.Use "Work"
		CreatDB_MsSQL = Dao.Work.CreatDB_MsSQL(DbIp, DbSamc, DbSapwd, DbName, DbUpmc, DbUpwd)
		On Error GoTo 0
	End function

	'------------------------------------------------------------------------------
	'# Ctrl.Make.CreatTable(ConnObj, Tabnamestr, CvArrstr, SqlType)
	'# @return: boolean
	'# @dowhat: 创建数据表
	'# 			创建成功返回 True 失败 False
	'--DESC------------------------------------------------------------------------
	'# @param ConnObj 		[object] : 数据库链接对象
	'# @param Tabnamestr 	[string] : 数据表名称
	'# @param CvArrstr 		[string] : 字段表 (写法： Fname1#Type#Len#Defvalue|Fname1#Type#Len#Defvalue|...) 最后一个不要写“|”
	'# @param SqlType 		[string] : Sql语句类型 (0 Access 1 Mssqlserver)
	'# ===Fname,Type,Len,Defvalue 说明：字段名称,字段类型,字段长度,默认值
	'# ===字段类型 Type：
	'#      C/c 字符 T/t 文本 I/i 二进制 D/d 日期 M/m
	'#      关键字(字符型) A/a 关键字自动编号(数值型) N/n 数值(float) Z/z 数值(int)
	'--DEMO------------------------------------------------------------------------
	'# Dim isMaked : isMaked = False
	'# AB.Use "Mvc" : Ctrl.Use "Make" : isMaked = Ctrl.Make.CreatTable(AB.db.Conn, "table01", "fi#a##|fa#t##|fb#c#20#a|fc#n##5", 0)
	'# If isMaked Then AB.C.Print "数据表创建成功" Else AB.C.Print "数据表创建失败"
	'#-----------------------------------------------------------------------------

	Public Function CreatTable(ByVal ConnObj,ByVal Tabnamestr,ByVal CvArrstr,ByVal SqlType)
		CreatTable = False
		On Error GoTo 0
		On Error Resume Next
		AB.Use "Mvc" : Dao.Use "Work"
		CreatTable = Dao.Work.CreatTable(ConnObj, Tabnamestr, CvArrstr, SqlType)
		On Error GoTo 0
	End Function

	'------------------------------------------------------------------------------
	'# Ctrl.Make.InterTBValue(ConnObj, Tabnamestr, CvArrstr)
	'# @return: integer
	'# @dowhat: 在数据库中插入字段值
	'# 			插入成功返回 True 失败 False
	'--DESC------------------------------------------------------------------------
	'# @param ConnObj 		[object] : 数据库链接对象
	'# @param Tabnamestr 	[string] : 数据表名称
	'# @param CvArrstr 		[string] : 字段表 (写法： Fname1#Value|Fname2#Value|...) 最后一个不要写“|”
	'# ===Fname,Value 说明：字段名称,字段值
	'--DEMO------------------------------------------------------------------------
	'# Dim isMaked : isMaked = False
	'# AB.Use "Mvc" : Ctrl.Use "Make" : isMaked = Ctrl.Make.InterTBValue(AB.db.Conn, "table02", "fa#x|fb#y|fc#z")
	'# If isMaked Then AB.C.Print "表插入数据成功" Else AB.C.Print "表插入数据失败"
	'#-----------------------------------------------------------------------------

	Public Function InterTBValue(ByVal ConnObj,ByVal Tabnamestr,ByVal CvArrstr)
		InterTBValue = False
		On Error GoTo 0
		On Error Resume Next
		AB.Use "Mvc" : Dao.Use "Work"
		InterTBValue = Dao.Work.InterTBValue(ConnObj, Tabnamestr, CvArrstr)
		On Error GoTo 0
	End Function

End Class
%>