<%
'######################################################################
'## service.common.asp
'## -------------------------------------------------------------------
'## Feature     :   AspBox Mvc Service-Common Block
'## Version     :   v1.0
'## Author      :   Lajox(lajox@19www.com)
'## Update Date :   2012/01/04 21:50
'## Description :   AspBox Mvc Service-Common Block(MVC服务器通用模块)
'######################################################################

Class Cls_Service_Common

	Private i

	Private Sub Class_Initialize()

	End Sub

	Private Sub Class_Terminate()

	End Sub

	'******************
	' 函数名: Service.Common.strRepeat
	'******************
	Public Function strRepeat(Byval str,Byval length)
		Dim i, temp
		For i = 1 To length
			temp = temp & str
		Next
		strRepeat = temp
	End Function

	'*************************************************
	' 函数名: Service.Common.EncodeJP
	' 参  数: str as the input string
	' 作  用: 编码日文
	'*************************************************
	Public Function EncodeJP(ByVal str)
		EncodeJP = str
		If str="" Then Exit Function
		Dim c1 : c1 = Array("ガ","ギ","グ","ア","ゲ","ゴ","ザ","ジ","ズ","ゼ","ゾ","ダ","ヂ","ヅ","デ","ド","バ","パ","ビ","ピ","ブ","プ","ベ","ペ","ボ","ポ","ヴ")
		Dim c2 : c2 = Array("460","462","463","450","466","468","470","472","474","476","478","480","482","485","487","489","496","497","499","500","502","503","505","506","508","509","532")
		Dim i
		For i=0 to 26
			str=Replace(str,c1(i),"&#12"&c2(i)&";")
		Next
		EncodeJP = str
	End Function

	'*************************************************
	' 函数名: Service.Common.HtmlEncode
	' 参  数: str as the input string
	' 作  用: filter html code
	'*************************************************
	Public Function HtmlEncode(ByVal Str)
		If Trim(Str) = "" Or IsNull(Str) Then
			HtmlEncode = ""
		Else
			str = Replace(str, "  ", "&nbsp; ")
			str = Replace(str, """", "&quot;")
			str = Replace(str, ">", "&gt;")
			str = Replace(str, "<", "&lt;")
			HtmlEncode = Str
		End If
	End Function

	'*************************************************
	' 函数名: Service.Common.HtmlDecode
	' 参  数: str as the input string
	' 作  用: Decode the html tag
	'*************************************************
	Public Function HtmlDecode(ByVal str)
		If Not IsNull(str) And str <> "" Then
			str = Replace(str, "&nbsp;", " ",1,-1,1)
			str = Replace(str, "&quot;", """",1,-1,1)
			str = Replace(str, "&gt;", ">",1,-1,1)
			str = Replace(str, "&lt;", "<",1,-1,1)
			HtmlDecode = str
		End If
	End Function

	'*************************************************
	' 函数名: Service.Common.UrlDecode
	' 作  用: UrlDecode — URL decode
	'*************************************************
	Public Function UrlDecode(ByVal vstrin)
		Dim i, strreturn, strSpecial, intasc, thischr
		strSpecial = "!""#$%&'()*+,.-_/:;<=>?@[\]^`{|}~%"
		strreturn = ""
		For i = 1 To Len(vstrin)
			thischr = Mid(vstrin, i, 1)
			If thischr = "%" Then
				intasc = Eval("&h" + Mid(vstrin, i + 1, 2))
				If InStr(strSpecial, Chr(intasc))>0 Then
					strreturn = strreturn & Chr(intasc)
					i = i + 2
				Else
					intasc = Eval("&h" + Mid(vstrin, i + 1, 2) + Mid(vstrin, i + 4, 2))
					strreturn = strreturn & Chr(intasc)
					i = i + 5
				End If
			Else
				If thischr = "+" Then
					strreturn = strreturn & " "
				Else
					strreturn = strreturn & thischr
				End If
			End If
		Next
		UrlDecode = strreturn
	End Function

	'*************************************************
	' 函数名: Service.Common.SetQueryString
	' 作用: 重置参数
	'*************************************************
	Public Function SetQueryString(ByVal sQuery, ByVal Name,ByVal Value)
		Dim Obj
		If Len(sQuery) > 0 Then
			If InStr(1,sQuery,Name&"=",1) = 0 Then
				If InStr(sQuery,"=") > 0 And Right(sQuery,1) <> "&" Then
					sQuery = sQuery & "&" & Name & "=" & Value
				Else
					sQuery = sQuery & Name & "=" & Value
				End If
			Else
				Set Obj = New Regexp
				Obj.IgnoreCase = False
				Obj.Global = True
				Obj.Pattern = "(&?" & Name & "=)[^&]+"
				sQuery = Obj.Replace(sQuery,"$1" & Value)
				Set Obj = Nothing
			End If
		Else
			sQuery = sQuery & Name & "=" & Value
		End If
		SetQueryString = sQuery
	End Function

	'*************************************************
	' 函数名: Service.Common.GetGUID
	' 作用: 生成GUID
	'*************************************************
	Public Function GetGUID()
		On Error Resume Next
		GetGUID = Mid(CreateObject("Scriptlet.TypeLib").Guid,2,36)
		Err.Clear
		On Error Goto 0
	End Function

End Class
%>