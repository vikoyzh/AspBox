<!--#include file="inc/AspBox/Cls_AB.asp" -->
<%
AB.Use "sc"
Dim sc : Set sc = ab.sc.new
sc.Lang = "js"
sc.Add "function getJsObj(){ var _tmp = {""name"": ""张三"", ""gender"": ""男"", ""result"": 65}; return _tmp; }"
sc.Add "function getJsArr(){ var _tmp = [""张三"", ""李四"", ""王五""]; return _tmp; }"
sc.Add "function getJsArr2(){ var _tmp = [""李四"", ""王五""]; return _tmp; }"
Dim jsobj : Set jsobj = sc.eval("getJsObj()")
Dim jsarr : jsarr = sc.eval("getJsArr()")
Dim jsarr2 : jsarr2 = sc.eval("getJsArr2()")

AB.Use "jsLib"
Dim jsLib : Set jsLib = AB.jsLib.New
jsLib.Inc("common.js")
Dim jso : Set jso = jsLib.Object
Dim str : str = ""

ab.c.printcn jso.count(jsobj) '@return: 3
ab.c.printcn jso.compareArray(jsarr, jsarr2) '@return: False
%>