<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include virtual="/Inc/AspBox/Cls_AB.asp" -->
<%
AB.Debug = True
AB.Use "Xml"
Dim str,n,i
str = "<?xml version=""1.0"" encoding=""utf-8""?>" & vbCrLf
str = str & "<microblog>" & vbCrLf
str = str & " <site>" & vbCrLf
str = str & " <name alias=""Tencent"">腾讯微博</name>" & vbCrLf
str = str & " <url>http://t.qq.com</url>" & vbCrLf
str = str & " <account nick=""user"" for=""me""><name>@qqid</name><nick>MyGod</nick></account>" & vbCrLf
str = str & " <last>"&"<"&"![CDATA[今天我们这里下<em>大雨</em>啦！]]></last></site>" & vbCrLf
str = str & " <site>" & vbCrLf
str = str & " <name alias=""Sina"">新浪微博</name>" & vbCrLf
str = str & " <url>http://t.sina.com.cn</url>" & vbCrLf
str = str & " <account nick=""email"" for=""me""><name>@sinaid</name><nick>testid@sina.com</nick></account>" & vbCrLf
str = str & " <last><"&"![CDATA[是不是<font color=""red"">这样</font>的噢，我也不知道哈。]]></last></site>" & vbCrLf
str = str & " <site>" & vbCrLf
str = str & " <name alias=""Twitter"">推特</name>" & vbCrLf
str = str & " <url>http://twitter.com</url>" & vbCrLf
str = str & " <account nick=""user"" for=""notme""><name haha=""1"">@twid</name><nick>TwId</nick></account>" & vbCrLf
str = str & " <last><"&"![CDATA[I don't need this feature <strong>(>_<)</strong> any more.]]></last></site>" & vbCrLf
str = str & "</microblog>"

'载入Xml数据
'AB.Xml.Load "/xmlfiles/microblog_catalog.xml"
AB.Xml.Load str
''选择所有标签为name的节点，并输出找到的节点个数
AB.C.WNH "标签为name的节点个数："& AB.Xml("name").Length
'AB.C.WR "--------"
''选择所有包含属性alias的标签为name的节点
AB.C.WNH "标签为name(包含属性alias)的节点个数："& AB.Xml("name[alias]").Length
'AB.C.WR "--------"
''选择所有属性for等于me，nick属性不等于email的标签为account的节点，并输出其Xml代码
AB.C.WNH "输出特定节点的Xml代码："& AB.Xml("account[for='me'][nick!='email']").Xml
AB.C.WR "--------"
''选择site节点的子节点中标签为name的节点
AB.C.WR "选择site节点的子节点中标签为name的节点的Xml代码："
AB.C.WNH AB.Xml("site>name").Xml
AB.C.WR "--------"
''选择account节点的后代节点中标签为name的节点
AB.C.WR "选择account节点的后代节点中标签为name的节点："
AB.C.WNH AB.Xml("account name").Xml
AB.C.WR "--------"
''选择所有的url和last节点
AB.C.WR "选择所有的url和last节点："
AB.C.WNH AB.Xml("url,last").Xml
AB.C.WR "--------"
AB.C.WNH AB.Xml("url")(2).Xml
AB.Xml("url")(2).Text = "<test>sss</test>"
AB.C.WNH AB.Xml("url")(2).Xml

'AB.Xml.XSLT = "xsl/microblog.xsl"
'AB.C.WNH AB.Xml.Dom.Xml

'AB.C.WR AB.Xml.SaveAs("news.xml>utf-8")
'AB.C.WR AB.Xml.SaveAs("microblog.xml>utf-8")

'Set n = AB.Xml("title")
'For i = 0 To n.Length-1
' AB.C.WR n(i).Value
'Next
'Set n = Nothing

'AB.C.WR AB.Xml("last")(2).Value
'Set n = AB.Xml("last")
'For i = 0 To n.Length-1
' 'AB.C.WR n(i).Type
' AB.C.WR n(i).Value
'Next
'AB.C.WR n.Text
'AB.C.WR n(1).Root.Type
'AB.C.WR n(2).Parent.Name
'AB.C.WR n(0).Clone(1).Text
'Set n = Nothing
'AB.Xml("name")(0).RemoveAttr("alias")
'AB.C.WNH AB.Xml("name")(0).Xml
'AB.Xml("site")(1).Clear
'AB.C.WNH AB.Xml("site")(1).Xml

'AB.C.WNH TypeName(AB.Xml("site")(0).Parent.Parent.Dom)
'AB.Xml("url").Remove
'AB.Xml("name").Attr("alias") = Null
'AB.Xml("microblog").Remove
'AB.C.WR AB.Xml.Sel("//site").Length
'AB.C.WR AB.Xml.Select("//site").Length
'AB.C.WR AB.Xml("site").Length
'AB.C.WR AB.Xml("site").Type
'AB.Xml("url")(2).Value = "http://sss.com"
'AB.C.WR TypeName(n)
'替换节点
'Set n = AB.Xml("name")(1).ReplaceWith(AB.Xml.Create("abbr cdata","This is a <b>word</b>."))
'Set n = AB.Xml("name").ReplaceWith(AB.Xml.Create("abbr cdata","This is a <b>word</b>."))
'Set n = AB.Xml("name")(1).ReplaceWith(AB.Xml("url")(2))
'AB.C.WNH n.Xml
'清空
'AB.Xml("url").Empty
'AB.Xml("name").Clear
'从前面加入节点
'Set n = AB.Xml("account")(1).Before(AB.Xml.Create("abbr cdata","This is a <b>word</b>."))
'Set n = AB.Xml("account")(1).Before(AB.Xml("url")(2))
'Set n = AB.Xml("account").Before(AB.Xml.Create("abbr cdata","This is a <b>word</b>."))
'Set n = AB.Xml("account").Before(AB.Xml("url")(2))
'从后面加入节点
'Set n = AB.Xml("account")(2).After(AB.Xml.Create("abbr cdata","This is a <b>word</b>."))
'Set n = AB.Xml("last")(1).After(AB.Xml.Create("abbr cdata","This is a <b>word</b>."))
'Set n = AB.Xml("account")(1).After(AB.Xml("url")(2))
'Set n = AB.Xml("account").After(AB.Xml.Create("abbr cdata","This is a <b>word</b>."))
'Set n = AB.Xml("account").After(AB.Xml("url")(2))


'AB.C.WNH n.Xml
'AB.C.WNH AB.Xml.Dom.Xml

'AB.C.WNH AB.Xml("name").Length
'AB.C.WNH AB.Xml("site name").Length
'AB.C.WNH AB.Xml("site>name").Length
'AB.C.WNH AB.Xml("name[alias='Tencent'],url").Length
'AB.C.WNH AB.Xml("name[alias='Tencent'],url").Text
'AB.C.WNH AB.Xml.Select("//account[@nick='user' and position()<2]").Length
'AB.C.WNH AB.Xml.Select("//account[@nick='user' and position()<2]").Xml
'AB.C.WNH AB.Xml("account[nick='user'][for!='me'],account[nick!='user']").Xml

'AB.C.WNH AB.Xml("site")(1).Find("account").Root.TypeString
'AB.C.WNH AB.Xml.Root.TypeString

'Set n = Nothing
AB.C.WR ""
AB.C.WR "------------------------------------"
AB.C.WR "页面执行时间： " & AB.C.GetScriptTime(0) & " 毫秒"
%>