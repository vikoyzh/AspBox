<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include virtual="/Inc/AspBox/Cls_AB.asp" -->
<%
Response.Charset = "UTF-8"
Response.CodePage = 65001
%>
<html>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
<meta http-equiv='refresh' content='1000'>
<title>javascript asp 获取并实时显示服务器时间</title>
<script language=javascript type='text/javascript'>
//特别注明：北京时间是格林尼治尺度时加八小时，因此用的起始时间也是从八点起算的
var secondServer = <%=DateDiff("s", "1970-01-01 08:00:00", Now())%>;
var secondClient = parseInt(new Date().getTime()/1000);
var secondSub = secondServer - secondClient; //两端时间秒差
function fm(s){return s < 10 ? '0' + s : s;}

function strTime(p, t)
{
	var y = t.getYear();//年
	var yy = t.getFullYear();//年(四位)
	var m = t.getMonth()+1;//月分(需加1)
	var mm = fm(m); //月分(两位)
	var d = t.getDate();//日
	var dd = fm(d); //日(两位)
	var wk = ['日','一','二','三','四','五','六'];
	var w = wk[t.getDay()]//星期
	var h = fm(t.getHours()); //时
	var hh = fm(h); //时(两位)
	var n = fm(t.getMinutes()); //分
	var nn = fm(n); //分(两位)
	var s = fm(t.getSeconds()); //秒
	var ss = fm(s); //秒(两位)
	p = p.replace('yyyy',yy).replace('Y',yy).replace('yy',yy).replace('y',y)
		.replace('mm',mm).replace('M',mm).replace('m',m)
		.replace('dd',dd).replace('D',dd).replace('d',d)
		.replace('hh',hh).replace('h',h)
		.replace('nn',nn).replace('n',n)
		.replace('ss',ss).replace('s',s)
		.replace('w',w);
	return p; //显示年月日和时间
}

function clockTime(n)
{
	var p = 'yyyy年mm月dd日 星期w h:n:s'
	var tm = new Date();
	tm.setTime(tm.getTime() + n*1000); //得到一个新的时间
	var t1 = strTime(p,tm);
	//var t2 = (new Date()).toString();
	var t2 = strTime(p,new Date());
	document.getElementById("servertime").value = t1;
	document.getElementById("localtime").value = t2;
	setTimeout('clockTime(secondSub)', 1000);
}
</script>
</head>
<body onLoad='clockTime(secondSub)'>
<%
Dim time : time = Now()
time = Year(time)&"-"&Month(time)&"-"&Hour(time)&" "&Hour(time)&":"&Minute(time)&":"&Second(time)
%>
当前服务器时间： <%=time%> <input id='servertime' size='40' readonly>
<br>当前本地电脑时间：<script>document.write(strTime('y-m-d h:n:s',new Date()));</script> <input id='localtime' size='40' readonly>
<hr>
<%
Function Mark(n)
	If InStr(""&n,"-") <= 0 Then Mark = "+" & n Else Mark = "-" & n
End Function

Dim sc, jso
'Set sc = CreateObject("ScriptControl")
Set sc = Server.CreateObject("MSScriptControl.ScriptControl")
sc.Language = "JScript"
sc.AddCode "var localTimezone = (new Date().getTimezoneOffset()/60)*(-1);"
sc.AddCode "var secondServer = " & DateDiff("s", "1970-01-01 08:00:00", Now()) & ";"
sc.AddCode "var secondClient = parseInt(new Date().getTime()/1000);"
sc.AddCode "var secondSub = secondServer - secondClient;"
sc.AddCode "var time = new Date(); time.setTime(time.getTime() + secondSub*1000);"
sc.AddCode "var serverTimezone = (time.getTimezoneOffset()/60)*(-1);"
Set jso = sc.CodeObject
Dim ServerTimezone, LocalTimezone
'ServerTimezone = jso.serverTimezone
'LocalTimezone = jso.localTimezone

AB.Use "Time"
ServerTimezone = AB.Time.ServerTimezone
LocalTimezone = AB.Time.LocalTimezone

Response.Write "当前服务器时区：" & Mark(ServerTimezone) & "<br>"
Response.Write "当前本地电脑时区：" & Mark(LocalTimezone) & "<br>"
%>
</body>
</html>